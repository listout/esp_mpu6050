#include <stdio.h>

#include "driver/i2c.h"
#include "esp_log.h"
#include "esp_system.h"
#include "i2c_bus.h"
#include "mpu6050.h"

#define I2C_MASTER_SCL_IO  22 /*!< gpio number for I2C master clock */
#define I2C_MASTER_SDA_IO  21 /*!< gpio number for I2C master data  */
#define I2C_MASTER_NUM	   I2C_NUM_1 /*!< I2C port number for master dev */
#define I2C_MASTER_FREQ_HZ 100000 /*!< I2C master clock frequency */

static i2c_bus_handle_t i2c_bus = NULL;
static mpu6050_handle_t mpu6050 = NULL;

static void mpu6050_init()
{
	i2c_config_t conf = {
		.mode = I2C_MODE_MASTER,
		.sda_io_num = I2C_MASTER_SDA_IO,
		.sda_pullup_en = GPIO_PULLUP_ENABLE,
		.scl_io_num = I2C_MASTER_SCL_IO,
		.scl_pullup_en = GPIO_PULLUP_ENABLE,
		.master.clk_speed = I2C_MASTER_FREQ_HZ,
	};
	i2c_bus = i2c_bus_create(I2C_MASTER_NUM, &conf);
	mpu6050 = mpu6050_create(i2c_bus, MPU6050_I2C_ADDRESS);
}

static void mpu6050_deinit()
{
	mpu6050_delete(&mpu6050);
	i2c_bus_delete(&i2c_bus);
}

/*
 *static void mpu6050_get_data()
 *{
 *        uint8_t mpu6050_deviceid;
 *        mpu6050_acce_value_t acce;
 *        mpu6050_gyro_value_t gyro;
 *        int cnt = 1;
 *        mpu6050_get_deviceid(mpu6050, &mpu6050_deviceid);
 *        printf("mpu6050 device ID is: 0x%02x\n", mpu6050_deviceid);
 *        mpu6050_wake_up(mpu6050);
 *        mpu6050_set_acce_fs(mpu6050, ACCE_FS_4G);
 *        mpu6050_set_gyro_fs(mpu6050, GYRO_FS_500DPS);
 *        while (cnt--) {
 *                printf("\n************* MPU6050 MOTION SENSOR ************\n");
 *                mpu6050_get_acce(mpu6050, &acce);
 *                printf("acce_x:%.2f, acce_y:%.2f, acce_z:%.2f\n", acce.acce_x, acce.acce_y,
 *                       acce.acce_z);
 *                mpu6050_get_gyro(mpu6050, &gyro);
 *                printf("gyro_x:%.2f, gyro_y:%.2f, gyro_z:%.2f\n", gyro.gyro_x, gyro.gyro_y,
 *                       gyro.gyro_z);
 *                printf("**************************************************\n");
 *                vTaskDelay(1000 / portTICK_RATE_MS);
 *        }
 *}
 */

void app_main(void)
{
	mpu6050_init();
	vTaskDelay(1000 / portTICK_PERIOD_MS);

	uint8_t mpu6050_deviceid;
	mpu6050_get_deviceid(mpu6050, &mpu6050_deviceid);
	printf("mpu6050 device ID is: 0x%02x\n", mpu6050_deviceid);
	mpu6050_wake_up(mpu6050);
	mpu6050_set_acce_fs(mpu6050, ACCE_FS_2G);
	mpu6050_set_gyro_fs(mpu6050, GYRO_FS_250DPS);

	mpu6050_set_acce_poweron_delay(mpu6050, MPU6050_NO_DELAY);
	mpu6050_set_int_freefall_enable(mpu6050, false);
	mpu6050_set_int_zero_motion_enabled(mpu6050, false);
	mpu6050_set_int_motion_enabled(mpu6050, true);
	mpu6050_set_dhpf_mode(mpu6050, MPU6050_DHPF_5HZ);
	mpu6050_set_motion_detection_threshold(mpu6050, 1);
	mpu6050_set_motion_detection_duration(mpu6050, 1);
	mpu6050_set_zero_motion_detection_threshold(mpu6050, 1);
	mpu6050_set_zero_motion_detection_duration(mpu6050, 1);
	mpu6050_set_dmp_enabled(mpu6050, true);
	mpu6050_check_settings(mpu6050);

	uint8_t *buffer = malloc(sizeof(uint8_t *) * 3);
	printf("%d\n", buffer[0]);
	float arr[20];
	int i = 0;

	while (1) {
		float maxi = arr[0], mini = arr[0], minii = arr[0], maxii = arr[0];

		mpu6050_acce_value_t acce;
		mpu6050_get_acce(mpu6050, &acce);
		arr[i++] = acce.acce_z;
		if (i >= 20) {
			for (int ii = 0; ii < 20; ii++) {
				if (arr[ii] > maxii) {
					maxii = arr[ii];
				}
				if (arr[ii] < minii) {
					minii = arr[ii];
				}
			}
			float out[20];
			for (int j = 0; j < 19; ++j) {
				out[j] = arr[i + 1] - arr[i];
			}
			for (int ii = 0; ii < 19; ii++) {
				if (out[ii] > maxi) {
					maxi = out[ii];
				}
				if (out[ii] < mini) {
					mini = out[ii];
				}
			}
			printf("maxii: %1.2f minii: %1.2f\n", maxii, minii);
			printf("maxi: %1.2f mini: %1.2f\n", maxi, mini);
			maxii = 0.0;
			minii = 0.0;
			maxi = 0.0;
			mini = 0.0;
			i = 0;
		}

		/*
		 *mpu6050_get_z_pos_motion_detected(mpu6050, buffer);
		 *if (buffer[0])
		 *        ESP_LOGI("main", "motion along z axis in positive direction");
		 *mpu6050_get_z_neg_motion_detected(mpu6050, buffer);
		 *if (buffer[0])
		 *        ESP_LOGI("main", "motion along z axis in neg direction");
		 */
		vTaskDelay(100 / portTICK_PERIOD_MS);
	}

	mpu6050_deinit();
}
