// Copyright 2015-2016 Espressif Systems (Shanghai) PTE LTD
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#ifndef __MPU6050_H_
#define __MPU6050_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "i2c_bus.h"

#define MPU6050_I2C_ADDRESS 0x69 /*!< slave address for MPU6050 sensor */

/* MPU6050 register */
#define MPU6050_SELF_TEST_X	   0x0D
#define MPU6050_SELF_TEST_Y	   0x0E
#define MPU6050_SELF_TEST_Z	   0x0F
#define MPU6050_SELF_TEST_A	   0x10
#define MPU6050_SMPLRT_DIV	   0x19
#define MPU6050_CONFIG		   0x1A
#define MPU6050_GYRO_CONFIG	   0x1B
#define MPU6050_ACCEL_CONFIG	   0x1C
#define MPU6050_FIFO_EN		   0x23
#define MPU6050_I2C_MST_CTRL	   0x24
#define MPU6050_I2C_SLV0_ADDR	   0x25
#define MPU6050_I2C_SLV0_REG	   0x26
#define MPU6050_I2C_SLV0_CTRL	   0x27
#define MPU6050_I2C_SLV1_ADDR	   0x28
#define MPU6050_I2C_SLV1_REG	   0x29
#define MPU6050_I2C_SLV1_CTRL	   0x2A
#define MPU6050_I2C_SLV2_ADDR	   0x2B
#define MPU6050_I2C_SLV2_REG	   0x2C
#define MPU6050_I2C_SLV2_CTRL	   0x2D
#define MPU6050_I2C_SLV3_ADDR	   0x2E
#define MPU6050_I2C_SLV3_REG	   0x2F
#define MPU6050_I2C_SLV3_CTRL	   0x30
#define MPU6050_I2C_SLV4_ADDR	   0x31
#define MPU6050_I2C_SLV4_REG	   0x32
#define MPU6050_I2C_SLV4_DO	   0x33
#define MPU6050_I2C_SLV4_CTRL	   0x34
#define MPU6050_I2C_SLV4_DI	   0x35
#define MPU6050_I2C_MST_STATUS	   0x36
#define MPU6050_INT_PIN_CFG	   0x37
#define MPU6050_INT_ENABLE	   0x38
#define MPU6050_DMP_INT_STATUS	   0x39
#define MPU6050_INT_STATUS	   0x3A
#define MPU6050_ACCEL_XOUT_H	   0x3B
#define MPU6050_ACCEL_XOUT_L	   0x3C
#define MPU6050_ACCEL_YOUT_H	   0x3D
#define MPU6050_ACCEL_YOUT_L	   0x3E
#define MPU6050_ACCEL_ZOUT_H	   0x3F
#define MPU6050_ACCEL_ZOUT_L	   0x40
#define MPU6050_TEMP_OUT_H	   0x41
#define MPU6050_TEMP_OUT_L	   0x42
#define MPU6050_GYRO_XOUT_H	   0x43
#define MPU6050_GYRO_XOUT_L	   0x44
#define MPU6050_GYRO_YOUT_H	   0x45
#define MPU6050_GYRO_YOUT_L	   0x46
#define MPU6050_GYRO_ZOUT_H	   0x47
#define MPU6050_GYRO_ZOUT_L	   0x48
#define MPU6050_EXT_SENS_DATA_00   0x49
#define MPU6050_EXT_SENS_DATA_01   0x4A
#define MPU6050_EXT_SENS_DATA_02   0x4B
#define MPU6050_EXT_SENS_DATA_03   0x4C
#define MPU6050_EXT_SENS_DATA_04   0x4D
#define MPU6050_EXT_SENS_DATA_05   0x4E
#define MPU6050_EXT_SENS_DATA_06   0x4F
#define MPU6050_EXT_SENS_DATA_07   0x50
#define MPU6050_EXT_SENS_DATA_08   0x51
#define MPU6050_EXT_SENS_DATA_09   0x52
#define MPU6050_EXT_SENS_DATA_10   0x53
#define MPU6050_EXT_SENS_DATA_11   0x54
#define MPU6050_EXT_SENS_DATA_12   0x55
#define MPU6050_EXT_SENS_DATA_13   0x56
#define MPU6050_EXT_SENS_DATA_14   0x57
#define MPU6050_EXT_SENS_DATA_15   0x58
#define MPU6050_EXT_SENS_DATA_16   0x59
#define MPU6050_EXT_SENS_DATA_17   0x5A
#define MPU6050_EXT_SENS_DATA_18   0x5B
#define MPU6050_EXT_SENS_DATA_19   0x5C
#define MPU6050_EXT_SENS_DATA_20   0x5D
#define MPU6050_EXT_SENS_DATA_21   0x5E
#define MPU6050_EXT_SENS_DATA_22   0x5F
#define MPU6050_EXT_SENS_DATA_23   0x60
#define MPU6050_I2C_SLV0_DO	   0x63
#define MPU6050_I2C_SLV1_DO	   0x64
#define MPU6050_I2C_SLV2_DO	   0x65
#define MPU6050_I2C_SLV3_DO	   0x66
#define MPU6050_I2C_MST_DELAY_CTRL 0x67
#define MPU6050_SIGNAL_PATH_RESET  0x68
#define MPU6050_USER_CTRL	   0x6A
#define MPU6050_PWR_MGMT_1	   0x6B
#define MPU6050_PWR_MGMT_2	   0x6C
#define MPU6050_FIFO_COUNTH	   0x72
#define MPU6050_FIFO_COUNTL	   0x73
#define MPU6050_FIFO_R_W	   0x74
#define MPU6050_WHO_AM_I	   0x75
#define MPU6050_MOT_THR		   0x1F
#define MPU6050_MOT_DUR		   0x20
#define MPU6050_ZRMOT_THR	   0x21
#define MPU6050_ZRMOT_DUR	   0x22
#define MPU6050_MOT_DETECT_CTRL	   0x69

#define MPU6050_XG_OFFS_TC	   0x00 //[7] PWR_MODE, [6:1] XG_OFFS_TC, [0] OTP_BNK_VLD
#define MPU6050_YG_OFFS_TC	   0x01 //[7] PWR_MODE, [6:1] YG_OFFS_TC, [0] OTP_BNK_VLD
#define MPU6050_ZG_OFFS_TC	   0x02 //[7] PWR_MODE, [6:1] ZG_OFFS_TC, [0] OTP_BNK_VLD
#define MPU6050_X_FINE_GAIN	   0x03 //[7:0] X_FINE_GAIN
#define MPU6050_Y_FINE_GAIN	   0x04 //[7:0] Y_FINE_GAIN
#define MPU6050_Z_FINE_GAIN	   0x05 //[7:0] Z_FINE_GAIN
#define MPU6050_XA_OFFS_H	   0x06 //[15:0] XA_OFFS
#define MPU6050_XA_OFFS_L_TC	   0x07
#define MPU6050_YA_OFFS_H	   0x08 //[15:0] YA_OFFS
#define MPU6050_YA_OFFS_L_TC	   0x09
#define MPU6050_ZA_OFFS_H	   0x0A //[15:0] ZA_OFFS
#define MPU6050_ZA_OFFS_L_TC	   0x0B
#define MPU6050_PRODUCT_ID	   0x0C
#define MPU6050_XG_OFFS_USRH	   0x13 //[15:0] XG_OFFS_USR
#define MPU6050_XG_OFFS_USRL	   0x14
#define MPU6050_YG_OFFS_USRH	   0x15 //[15:0] YG_OFFS_USR
#define MPU6050_YG_OFFS_USRL	   0x16
#define MPU6050_ZG_OFFS_USRH	   0x17 //[15:0] ZG_OFFS_USR
#define MPU6050_ZG_OFFS_USRL	   0x18
#define MPU6050_SMPLRT_DIV	   0x19
#define MPU6050_CONFIG		   0x1A
#define MPU6050_GYRO_CONFIG	   0x1B
#define MPU6050_ACCEL_CONFIG	   0x1C
#define MPU6050_FF_THR		   0x1D
#define MPU6050_FF_DUR		   0x1E
#define MPU6050_MOT_THR		   0x1F
#define MPU6050_MOT_DUR		   0x20
#define MPU6050_ZRMOT_THR	   0x21
#define MPU6050_ZRMOT_DUR	   0x22
#define MPU6050_FIFO_EN		   0x23
#define MPU6050_I2C_MST_CTRL	   0x24
#define MPU6050_I2C_SLV0_ADDR	   0x25
#define MPU6050_I2C_SLV0_REG	   0x26
#define MPU6050_I2C_SLV0_CTRL	   0x27
#define MPU6050_I2C_SLV1_ADDR	   0x28
#define MPU6050_I2C_SLV1_REG	   0x29
#define MPU6050_I2C_SLV1_CTRL	   0x2A
#define MPU6050_I2C_SLV2_ADDR	   0x2B
#define MPU6050_I2C_SLV2_REG	   0x2C
#define MPU6050_I2C_SLV2_CTRL	   0x2D
#define MPU6050_I2C_SLV3_ADDR	   0x2E
#define MPU6050_I2C_SLV3_REG	   0x2F
#define MPU6050_I2C_SLV3_CTRL	   0x30
#define MPU6050_I2C_SLV4_ADDR	   0x31
#define MPU6050_I2C_SLV4_REG	   0x32
#define MPU6050_I2C_SLV4_DO	   0x33
#define MPU6050_I2C_SLV4_CTRL	   0x34
#define MPU6050_I2C_SLV4_DI	   0x35
#define MPU6050_I2C_MST_STATUS	   0x36
#define MPU6050_INT_PIN_CFG	   0x37
#define MPU6050_INT_ENABLE	   0x38
#define MPU6050_DMP_INT_STATUS	   0x39
#define MPU6050_INT_STATUS	   0x3A
#define MPU6050_ACCEL_XOUT_H	   0x3B
#define MPU6050_ACCEL_XOUT_L	   0x3C
#define MPU6050_ACCEL_YOUT_H	   0x3D
#define MPU6050_ACCEL_YOUT_L	   0x3E
#define MPU6050_ACCEL_ZOUT_H	   0x3F
#define MPU6050_ACCEL_ZOUT_L	   0x40
#define MPU6050_TEMP_OUT_H	   0x41
#define MPU6050_TEMP_OUT_L	   0x42
#define MPU6050_GYRO_XOUT_H	   0x43
#define MPU6050_GYRO_XOUT_L	   0x44
#define MPU6050_GYRO_YOUT_H	   0x45
#define MPU6050_GYRO_YOUT_L	   0x46
#define MPU6050_GYRO_ZOUT_H	   0x47
#define MPU6050_GYRO_ZOUT_L	   0x48
#define MPU6050_EXT_SENS_DATA_00   0x49
#define MPU6050_EXT_SENS_DATA_01   0x4A
#define MPU6050_EXT_SENS_DATA_02   0x4B
#define MPU6050_EXT_SENS_DATA_03   0x4C
#define MPU6050_EXT_SENS_DATA_04   0x4D
#define MPU6050_EXT_SENS_DATA_05   0x4E
#define MPU6050_EXT_SENS_DATA_06   0x4F
#define MPU6050_EXT_SENS_DATA_07   0x50
#define MPU6050_EXT_SENS_DATA_08   0x51
#define MPU6050_EXT_SENS_DATA_09   0x52
#define MPU6050_EXT_SENS_DATA_10   0x53
#define MPU6050_EXT_SENS_DATA_11   0x54
#define MPU6050_EXT_SENS_DATA_12   0x55
#define MPU6050_EXT_SENS_DATA_13   0x56
#define MPU6050_EXT_SENS_DATA_14   0x57
#define MPU6050_EXT_SENS_DATA_15   0x58
#define MPU6050_EXT_SENS_DATA_16   0x59
#define MPU6050_EXT_SENS_DATA_17   0x5A
#define MPU6050_EXT_SENS_DATA_18   0x5B
#define MPU6050_EXT_SENS_DATA_19   0x5C
#define MPU6050_EXT_SENS_DATA_20   0x5D
#define MPU6050_EXT_SENS_DATA_21   0x5E
#define MPU6050_EXT_SENS_DATA_22   0x5F
#define MPU6050_EXT_SENS_DATA_23   0x60
#define MPU6050_MOT_DETECT_STATUS  0x61
#define MPU6050_I2C_SLV0_DO	   0x63
#define MPU6050_I2C_SLV1_DO	   0x64
#define MPU6050_I2C_SLV2_DO	   0x65
#define MPU6050_I2C_SLV3_DO	   0x66
#define MPU6050_I2C_MST_DELAY_CTRL 0x67
#define MPU6050_SIGNAL_PATH_RESET  0x68
#define MPU6050_MOT_DETECT_CTRL	   0x69
#define MPU6050_PWR_MGMT_1	   0x6B
#define MPU6050_PWR_MGMT_2	   0x6C
#define MPU6050_BANK_SEL	   0x6D
#define MPU6050_MEM_START_ADDR	   0x6E
#define MPU6050_MEM_R_W		   0x6F
#define MPU6050_DMP_CFG_1	   0x70
#define MPU6050_DMP_CFG_2	   0x71
#define MPU6050_FIFO_COUNTH	   0x72
#define MPU6050_FIFO_COUNTL	   0x73
#define MPU6050_FIFO_R_W	   0x74
#define MPU6050_WHO_AM_I	   0x75

#define MPU6050_INTERRUPT_FF_BIT	     7
#define MPU6050_INTERRUPT_MOT_BIT	     6
#define MPU6050_INTERRUPT_ZMOT_BIT	     5
#define MPU6050_INTERRUPT_FIFO_OFLOW_BIT     4
#define MPU6050_INTERRUPT_I2C_MST_INT_BIT    3
#define MPU6050_INTERRUPT_PLL_RDY_INT_BIT    2
#define MPU6050_INTERRUPT_DMP_INT_BIT	     1
#define MPU6050_INTERRUPT_DATA_RDY_BIT	     0
#define MPU6050_ACONFIG_XA_ST_BIT	     7
#define MPU6050_ACONFIG_YA_ST_BIT	     6
#define MPU6050_ACONFIG_ZA_ST_BIT	     5
#define MPU6050_ACONFIG_AFS_SEL_BIT	     4
#define MPU6050_ACONFIG_AFS_SEL_LENGTH	     2
#define MPU6050_ACONFIG_ACCEL_HPF_BIT	     2
#define MPU6050_ACONFIG_ACCEL_HPF_LENGTH     3
#define MPU6050_DETECT_ACCEL_ON_DELAY_BIT    5
#define MPU6050_DETECT_ACCEL_ON_DELAY_LENGTH 2
#define MPU6050_DETECT_FF_COUNT_BIT	     3
#define MPU6050_DETECT_FF_COUNT_LENGTH	     2
#define MPU6050_DETECT_MOT_COUNT_BIT	     1
#define MPU6050_DETECT_MOT_COUNT_LENGTH	     2

#define MPU6050_GCONFIG_XG_ST_BIT     7
#define MPU6050_GCONFIG_YG_ST_BIT     6
#define MPU6050_GCONFIG_ZG_ST_BIT     5
#define MPU6050_GCONFIG_FS_SEL_BIT    4
#define MPU6050_GCONFIG_FS_SEL_LENGTH 2

#define MPU6050_DETECT_DECREMENT_RESET 0x0
#define MPU6050_DETECT_DECREMENT_1     0x1
#define MPU6050_DETECT_DECREMENT_2     0x2
#define MPU6050_DETECT_DECREMENT_4     0x3

#define MPU6050_USERCTRL_DMP_EN_BIT	    7
#define MPU6050_USERCTRL_FIFO_EN_BIT	    6
#define MPU6050_USERCTRL_I2C_MST_EN_BIT	    5
#define MPU6050_USERCTRL_I2C_IF_DIS_BIT	    4
#define MPU6050_USERCTRL_DMP_RESET_BIT	    3
#define MPU6050_USERCTRL_FIFO_RESET_BIT	    2
#define MPU6050_USERCTRL_I2C_MST_RESET_BIT  1
#define MPU6050_USERCTRL_SIG_COND_RESET_BIT 0

#define MPU6050_PWR1_DEVICE_RESET_BIT 7
#define MPU6050_PWR1_SLEEP_BIT	      6
#define MPU6050_PWR1_CYCLE_BIT	      5
#define MPU6050_PWR1_TEMP_DIS_BIT     3
#define MPU6050_PWR1_CLKSEL_BIT	      2
#define MPU6050_PWR1_CLKSEL_LENGTH    3

#define MPU6050_CLOCK_INTERNAL	 0x00
#define MPU6050_CLOCK_PLL_XGYRO	 0x01
#define MPU6050_CLOCK_PLL_YGYRO	 0x02
#define MPU6050_CLOCK_PLL_ZGYRO	 0x03
#define MPU6050_CLOCK_PLL_EXT32K 0x04
#define MPU6050_CLOCK_PLL_EXT19M 0x05
#define MPU6050_CLOCK_KEEP_RESET 0x07

#define MPU6050_PWR2_LP_WAKE_CTRL_BIT	 7
#define MPU6050_PWR2_LP_WAKE_CTRL_LENGTH 2
#define MPU6050_PWR2_STBY_XA_BIT	 5
#define MPU6050_PWR2_STBY_YA_BIT	 4
#define MPU6050_PWR2_STBY_ZA_BIT	 3
#define MPU6050_PWR2_STBY_XG_BIT	 2
#define MPU6050_PWR2_STBY_YG_BIT	 1
#define MPU6050_PWR2_STBY_ZG_BIT	 0

#define MPU6050_WAKE_FREQ_1P25 0x0
#define MPU6050_WAKE_FREQ_2P5  0x1
#define MPU6050_WAKE_FREQ_5    0x2
#define MPU6050_WAKE_FREQ_10   0x3

#define MPU6050_BANKSEL_PRFTCH_EN_BIT	  6
#define MPU6050_BANKSEL_CFG_USER_BANK_BIT 5
#define MPU6050_BANKSEL_MEM_SEL_BIT	  4
#define MPU6050_BANKSEL_MEM_SEL_LENGTH	  5

#define MPU6050_ACCEL_FS_2  0x00
#define MPU6050_ACCEL_FS_4  0x01
#define MPU6050_ACCEL_FS_8  0x02
#define MPU6050_ACCEL_FS_16 0x03

#define MPU6050_GYRO_FS_250  0x00
#define MPU6050_GYRO_FS_500  0x01
#define MPU6050_GYRO_FS_1000 0x02
#define MPU6050_GYRO_FS_2000 0x03

#define MPU6050_TC_PWR_MODE_BIT	   7
#define MPU6050_TC_OFFSET_BIT	   6
#define MPU6050_TC_OFFSET_LENGTH   6
#define MPU6050_TC_OTP_BNK_VLD_BIT 0

// TODO: figure out what these actually do
// UMPL source code is not very obivous
#define MPU6050_DMPINT_5_BIT 5
#define MPU6050_DMPINT_4_BIT 4
#define MPU6050_DMPINT_3_BIT 3
#define MPU6050_DMPINT_2_BIT 2
#define MPU6050_DMPINT_1_BIT 1
#define MPU6050_DMPINT_0_BIT 0

#define MPU6050_MOTION_MOT_XNEG_BIT  7
#define MPU6050_MOTION_MOT_XPOS_BIT  6
#define MPU6050_MOTION_MOT_YNEG_BIT  5
#define MPU6050_MOTION_MOT_YPOS_BIT  4
#define MPU6050_MOTION_MOT_ZNEG_BIT  3
#define MPU6050_MOTION_MOT_ZPOS_BIT  2
#define MPU6050_MOTION_MOT_ZRMOT_BIT 0

#define MPU6050_DEG_PER_LSB_250	 (float)((2 * 250.0) / 65536.0)
#define MPU6050_DEG_PER_LSB_500	 (float)((2 * 500.0) / 65536.0)
#define MPU6050_DEG_PER_LSB_1000 (float)((2 * 1000.0) / 65536.0)
#define MPU6050_DEG_PER_LSB_2000 (float)((2 * 2000.0) / 65536.0)

#define MPU6050_G_PER_LSB_2  (float)((2 * 2) / 65536.0)
#define MPU6050_G_PER_LSB_4  (float)((2 * 4) / 65536.0)
#define MPU6050_G_PER_LSB_8  (float)((2 * 8) / 65536.0)
#define MPU6050_G_PER_LSB_16 (float)((2 * 16) / 65536.0)

#define MPU6050_CFG_EXT_SYNC_SET_BIT	5
#define MPU6050_CFG_EXT_SYNC_SET_LENGTH 3
#define MPU6050_CFG_DLPF_CFG_BIT	2
#define MPU6050_CFG_DLPF_CFG_LENGTH	3

#define MPU6050_TEMP_FIFO_EN_BIT  7
#define MPU6050_XG_FIFO_EN_BIT	  6
#define MPU6050_YG_FIFO_EN_BIT	  5
#define MPU6050_ZG_FIFO_EN_BIT	  4
#define MPU6050_ACCEL_FIFO_EN_BIT 3
#define MPU6050_SLV2_FIFO_EN_BIT  2
#define MPU6050_SLV1_FIFO_EN_BIT  1
#define MPU6050_SLV0_FIFO_EN_BIT  0

#define MPU6050_INTCFG_INT_LEVEL_BIT	   7
#define MPU6050_INTCFG_INT_OPEN_BIT	   6
#define MPU6050_INTCFG_LATCH_INT_EN_BIT	   5
#define MPU6050_INTCFG_INT_RD_CLEAR_BIT	   4
#define MPU6050_INTCFG_FSYNC_INT_LEVEL_BIT 3
#define MPU6050_INTCFG_FSYNC_INT_EN_BIT	   2
#define MPU6050_INTCFG_I2C_BYPASS_EN_BIT   1
#define MPU6050_INTCFG_CLKOUT_EN_BIT	   0

#define BANK_SIZE 256

/**
 * @brief Accelerometer high pass filter options
 *
 * Allowed values for `setHighPassFilter`.
 */
typedef enum {
	MPU6050_HIGHPASS_DISABLE,
	MPU6050_HIGHPASS_5_HZ,
	MPU6050_HIGHPASS_2_5_HZ,
	MPU6050_HIGHPASS_1_25_HZ,
	MPU6050_HIGHPASS_0_63_HZ,
	MPU6050_HIGHPASS_UNUSED,
	MPU6050_HIGHPASS_HOLD,
} mpu6050_highpass_t;

typedef enum {
	ACCE_FS_2G = 0, /*!< Accelerometer full scale range is +/- 2g */
	ACCE_FS_4G = 1, /*!< Accelerometer full scale range is +/- 4g */
	ACCE_FS_8G = 2, /*!< Accelerometer full scale range is +/- 8g */
	ACCE_FS_16G = 3, /*!< Accelerometer full scale range is +/- 16g */
} mpu6050_acce_fs_t;

typedef enum {
	GYRO_FS_250DPS = 0, /*!< Gyroscope full scale range is +/- 250 degree per sencond */
	GYRO_FS_500DPS = 1, /*!< Gyroscope full scale range is +/- 500 degree per sencond */
	GYRO_FS_1000DPS = 2, /*!< Gyroscope full scale range is +/- 1000 degree per sencond */
	GYRO_FS_2000DPS = 3, /*!< Gyroscope full scale range is +/- 2000 degree per sencond */
} mpu6050_gyro_fs_t;

typedef enum {
	MPU6050_NO_DELAY = 0,
	MPU6050_DELAY_1MS = 1,
	MPU6050_DELAY_2MS = 2,
	MPU6050_DELAY_3MS = 3,
} mpu6050_ondelay_t;

typedef enum {
	MPU6050_DHPF_HOLD = 7,
	MPU6050_DHPF_0_63HZ = 4,
	MPU6050_DHPF_1_25HZ = 3,
	MPU6050_DHPF_2_5HZ = 2,
	MPU6050_DHPF_5HZ = 1,
	MPU6050_DHPF_RESET = 0,
} mpu6050_dhpf_t;

typedef struct {
	int16_t raw_acce_x;
	int16_t raw_acce_y;
	int16_t raw_acce_z;
} mpu6050_raw_acce_value_t;

typedef struct {
	int16_t raw_gyro_x;
	int16_t raw_gyro_y;
	int16_t raw_gyro_z;
} mpu6050_raw_gyro_value_t;

typedef struct {
	float acce_x;
	float acce_y;
	float acce_z;
} mpu6050_acce_value_t;

typedef struct {
	float gyro_x;
	float gyro_y;
	float gyro_z;
} mpu6050_gyro_value_t;

typedef struct {
	float roll;
	float pitch;
} complimentary_angle_t;

typedef void *mpu6050_handle_t;

/**
 * @brief Create and init sensor object and return a sensor handle
 *
 * @param bus I2C bus object handle
 * @param dev_addr I2C device address of sensor
 *
 * @return
 *     - NULL Fail
 *     - Others Success
 */
mpu6050_handle_t mpu6050_create(i2c_bus_handle_t bus, uint8_t dev_addr);

/**
 * @brief Delete and release a sensor object
 *
 * @param sensor point to object handle of mpu6050
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_FAIL Fail
 */
esp_err_t mpu6050_delete(mpu6050_handle_t *sensor);

/**
 * @brief Get device identification of MPU6050
 *
 * @param sensor object handle of mpu6050
 * @param deviceid a pointer of device ID
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_FAIL Fail
 */
esp_err_t mpu6050_get_deviceid(mpu6050_handle_t sensor, uint8_t *deviceid);

/**
 * @brief Wake up MPU6050
 *
 * @param sensor object handle of mpu6050
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_FAIL Fail
 */
esp_err_t mpu6050_wake_up(mpu6050_handle_t sensor);

/**
 * @brief Enter sleep mode
 *
 * @param sensor object handle of mpu6050
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_FAIL Fail
 */
esp_err_t mpu6050_sleep(mpu6050_handle_t sensor);

/** Get Zero Motion Detection interrupt enabled status.
 * Will be set 0 for disabled, 1 for enabled.
 * @return Current interrupt enabled status
 * @see MPU6050_RA_INT_ENABLE
 * @see MPU6050_INTERRUPT_ZMOT_BIT
 **/
esp_err_t mpu6050_get_int_zero_motion_enabled(mpu6050_handle_t sensor, uint8_t *buffer);

/** Set Zero Motion Detection interrupt enabled status.
 * @param enabled New interrupt enabled status
 * @see getIntZeroMotionEnabled()
 * @see MPU6050_RA_INT_ENABLE
 * @see MPU6050_INTERRUPT_ZMOT_BIT
 **/
esp_err_t mpu6050_set_int_zero_motion_enabled(mpu6050_handle_t sensor, bool enabled);

/** Get Motion Detection interrupt enabled status.
 * Will be set 0 for disabled, 1 for enabled.
 * @return Current interrupt enabled status
 * @see MPU6050_RA_INT_ENABLE
 * @see MPU6050_INTERRUPT_MOT_BIT
 **/
esp_err_t mpu6050_get_int_motion_enabled(mpu6050_handle_t sensor, uint8_t *buffer);

/** Set Motion Detection interrupt enabled status.
 * @param enabled New interrupt enabled status
 * @see getIntMotionEnabled()
 * @see MPU6050_RA_INT_ENABLE
 * @see MPU6050_INTERRUPT_MOT_BIT
 **/
esp_err_t mpu6050_set_int_motion_enabled(mpu6050_handle_t sensor, bool enabled);

/** Get Motion Detection interrupt status.
 * This bit automatically sets to 1 when a Motion Detection interrupt has been
 * generated. The bit clears to 0 after the register has been read.
 * @return Current interrupt status
 * @see MPU6050_RA_INT_STATUS
 * @see MPU6050_INTERRUPT_MOT_BIT
 */
esp_err_t mpu6050_get_int_motion_status(mpu6050_handle_t, uint8_t *);

/** Get Zero Motion Detection interrupt status.
 * This bit automatically sets to 1 when a Zero Motion Detection interrupt has
 * been generated. The bit clears to 0 after the register has been read.
 * @return Current interrupt status
 * @see MPU6050_RA_INT_STATUS
 * @see MPU6050_INTERRUPT_ZMOT_BIT
 */
esp_err_t mpu6050_get_int_zero_motion_status(mpu6050_handle_t, uint8_t *);

/** Get full set of interrupt status bits.
 * These bits clear to 0 after the register has been read. Very useful
 * for getting multiple INT statuses, since each single bit read clears
 * all of them because it has to read the whole byte.
 * @return Current interrupt status
 * @see MPU6050_RA_INT_STATUS
 */
esp_err_t mpu6050_get_int_status(mpu6050_handle_t, uint8_t *);

/** Get zero motion detection event acceleration threshold.
 * This register configures the detection threshold for Zero Motion interrupt
 * generation. The unit of ZRMOT_THR is 1LSB = 2mg. Zero Motion is detected when
 * the absolute value of the accelerometer measurements for the 3 axes are each
 * less than the detection threshold. This condition increments the Zero Motion
 * duration counter (Register 34). The Zero Motion interrupt is triggered when
 * the Zero Motion duration counter reaches the time count specified in
 * ZRMOT_DUR (Register 34).
 *
 * Unlike Free Fall or Motion detection, Zero Motion detection triggers an
 * interrupt both when Zero Motion is first detected and when Zero Motion is no
 * longer detected.
 *
 * When a zero motion event is detected, a Zero Motion Status will be indicated
 * in the MOT_DETECT_STATUS register (Register 97). When a motion-to-zero-motion
 * condition is detected, the status bit is set to 1. When a zero-motion-to-
 * motion condition is detected, the status bit is set to 0.
 *
 * For more details on the Zero Motion detection interrupt, see Section 8.4 of
 * the MPU-6000/MPU-6050 Product Specification document as well as Registers 56
 * and 58 of this document.
 *
 * @return Current zero motion detection acceleration threshold value (LSB = 2mg)
 * @see MPU6050_RA_ZRMOT_THR
 */
esp_err_t mpu6050_get_zero_motion_detection_threshold(mpu6050_handle_t sensor, uint8_t *buffer);

/** Set zero motion detection event acceleration threshold.
 * @param threshold New zero motion detection acceleration threshold value (LSB = 2mg)
 * @see getZeroMotionDetectionThreshold()
 * @see MPU6050_RA_ZRMOT_THR
 */
esp_err_t mpu6050_set_zero_motion_detection_threshold(mpu6050_handle_t sensor, uint8_t buffer);

/** Get zero motion detection event duration threshold.
 * This register configures the duration counter threshold for Zero Motion
 * interrupt generation. The duration counter ticks at 16 Hz, therefore
 * ZRMOT_DUR has a unit of 1 LSB = 64 ms. The Zero Motion duration counter
 * increments while the absolute value of the accelerometer measurements are
 * each less than the detection threshold (Register 33). The Zero Motion
 * interrupt is triggered when the Zero Motion duration counter reaches the time
 * count specified in this register.
 *
 * For more details on the Zero Motion detection interrupt, see Section 8.4 of
 * the MPU-6000/MPU-6050 Product Specification document, as well as Registers 56
 * and 58 of this document.
 *
 * @return Current zero motion detection duration threshold value (LSB = 64ms)
 * @see MPU6050_RA_ZRMOT_DUR
 */
esp_err_t mpu6050_get_zero_motion_detection_duration(mpu6050_handle_t sensor, uint8_t *buffer);

/** Set zero motion detection event duration threshold.
 * @param duration New zero motion detection duration threshold value (LSB = 1ms)
 * @see getZeroMotionDetectionDuration()
 * @see MPU6050_RA_ZRMOT_DUR
 */
esp_err_t mpu6050_set_zero_motion_detection_duration(mpu6050_handle_t sensor, uint8_t buffer);

/** Get motion detection event acceleration threshold.
 * This register configures the detection threshold for Motion interrupt
 * generation. The unit of MOT_THR is 1LSB = 2mg. Motion is detected when the
 * absolute value of any of the accelerometer measurements exceeds this Motion
 * detection threshold. This condition increments the Motion detection duration
 * counter (Register 32). The Motion detection interrupt is triggered when the
 * Motion Detection counter reaches the time count specified in MOT_DUR
 * (Register 32).
 *
 * The Motion interrupt will indicate the axis and polarity of detected motion
 * in MOT_DETECT_STATUS (Register 97).
 *
 * For more details on the Motion detection interrupt, see Section 8.3 of the
 * MPU-6000/MPU-6050 Product Specification document as well as Registers 56 and
 * 58 of this document.
 *
 * @return Current motion detection acceleration threshold value (LSB = 2mg)
 * @see MPU6050_RA_MOT_THR
 */
esp_err_t mpu6050_get_motion_detection_threshold(mpu6050_handle_t sensor, uint8_t *buffer);

/** Set motion detection event acceleration threshold.
 * @param threshold New motion detection acceleration threshold value (LSB = 2mg)
 * @see getMotionDetectionThreshold()
 * @see MPU6050_RA_MOT_THR
 */
esp_err_t mpu6050_set_motion_detection_threshold(mpu6050_handle_t sensor, uint8_t buffer);

/** Set motion detection event duration threshold.
 * @param duration New motion detection duration threshold value (LSB = 1ms)
 * @see getMotionDetectionDuration()
 * @see MPU6050_RA_MOT_DUR
 */
esp_err_t mpu6050_set_motion_detection_duration(mpu6050_handle_t sensor, uint8_t buffer);

/** Get motion detection event duration threshold.
 * This register configures the duration counter threshold for Motion interrupt
 * generation. The duration counter ticks at 1 kHz, therefore MOT_DUR has a unit
 * of 1LSB = 1ms. The Motion detection duration counter increments when the
 * absolute value of any of the accelerometer measurements exceeds the Motion
 * detection threshold (Register 31). The Motion detection interrupt is
 * triggered when the Motion detection counter reaches the time count specified
 * in this register.
 *
 * For more details on the Motion detection interrupt, see Section 8.3 of the
 * MPU-6000/MPU-6050 Product Specification document.
 *
 * @return Current motion detection duration threshold value (LSB = 1ms)
 * @see MPU6050_RA_MOT_DUR
 */
esp_err_t mpu6050_get_motion_detection_duration(mpu6050_handle_t sensor, uint8_t *buffer);

/** Set the high-pass filter configuration.
 * @param bandwidth New high-pass filter configuration
 * @see setDHPFMode()
 * @see MPU6050_DHPF_RESET
 * @see MPU6050_RA_ACCEL_CONFIG
 */
esp_err_t mpu6050_set_dhpf_mode(mpu6050_handle_t sensor, uint8_t buffer);

/** Get the high-pass filter configuration.
 * The DHPF is a filter module in the path leading to motion detectors (Free
 * Fall, Motion threshold, and Zero Motion). The high pass filter output is not
 * available to the data registers (see Figure in Section 8 of the MPU-6000/
 * MPU-6050 Product Specification document).
 *
 * The high pass filter has three modes:
 *
 * <pre>
 *    Reset: The filter output settles to zero within one sample. This
 *           effectively disables the high pass filter. This mode may be toggled
 *           to quickly settle the filter.
 *
 *    On:    The high pass filter will pass signals above the cut off frequency.
 *
 *    Hold:  When triggered, the filter holds the present sample. The filter
 *           output will be the difference between the input sample and the held
 *           sample.
 * </pre>
 *
 * <pre>
 * ACCEL_HPF | Filter Mode | Cut-off Frequency
 * ----------+-------------+------------------
 * 0         | Reset       | None
 * 1         | On          | 5Hz
 * 2         | On          | 2.5Hz
 * 3         | On          | 1.25Hz
 * 4         | On          | 0.63Hz
 * 7         | Hold        | None
 * </pre>
 *
 * @return Current high-pass filter configuration
 * @see MPU6050_DHPF_RESET
 * @see MPU6050_RA_ACCEL_CONFIG
 */
esp_err_t mpu6050_get_dhpf_mode(mpu6050_handle_t sensor, uint8_t *buffer);

/** Get accelerometer power-on delay.
 * The accelerometer data path provides samples to the sensor registers, Motion
 * detection, Zero Motion detection, and Free Fall detection modules. The
 * signal path contains filters which must be flushed on wake-up with new
 * samples before the detection modules begin operations. The default wake-up
 * delay, of 4ms can be lengthened by up to 3ms. This additional delay is
 * specified in ACCEL_ON_DELAY in units of 1 LSB = 1 ms. The user may select
 * any value above zero unless instructed otherwise by InvenSense. Please refer
 * to Section 8 of the MPU-6000/MPU-6050 Product Specification document for
 * further information regarding the detection modules.
 * @return Current accelerometer power-on delay
 * @see MPU6050_RA_MOT_DETECT_CTRL
 * @see MPU6050_DETECT_ACCEL_ON_DELAY_BIT
 */
esp_err_t mpu6050_get_acce_poweron_delay(mpu6050_handle_t sensor, uint8_t *buffer);

/** Set accelerometer power-on delay.
 * @param delay New accelerometer power-on delay (0-3)
 * @see getAccelerometerPowerOnDelay()
 * @see MPU6050_RA_MOT_DETECT_CTRL
 * @see MPU6050_DETECT_ACCEL_ON_DELAY_BIT
 */
esp_err_t mpu6050_set_acce_poweron_delay(mpu6050_handle_t sensor, uint8_t buffer);

/** Get Free Fall interrupt enabled status.
 * Will be set 0 for disabled, 1 for enabled.
 * @return Current interrupt enabled status
 * @see MPU6050_RA_INT_ENABLE
 * @see MPU6050_INTERRUPT_FF_BIT
 **/
esp_err_t mpu6050_get_int_freefall_enable(mpu6050_handle_t sensor, uint8_t *buffer);

/** Set Free Fall interrupt enabled status.
 * @param enabled New interrupt enabled status
 * @see getIntFreefallEnabled()
 * @see MPU6050_RA_INT_ENABLE
 * @see MPU6050_INTERRUPT_FF_BIT
 **/
esp_err_t mpu6050_set_int_freefall_enable(mpu6050_handle_t sensor, bool enabled);

/** Get sleep mode status.
 * Setting the SLEEP bit in the register puts the device into very low power
 * sleep mode. In this mode, only the serial interface and internal registers
 * remain active, allowing for a very low standby current. Clearing this bit
 * puts the device back into normal mode. To save power, the individual standby
 * selections for each of the gyros should be used if any gyro axis is not used
 * by the application.
 * @return Current sleep mode enabled status
 * @see MPU6050_RA_PWR_MGMT_1
 * @see MPU6050_PWR1_SLEEP_BIT
 */
esp_err_t mpu6050_get_sleep_enable(mpu6050_handle_t sensor, uint8_t *buffer);

/** Set sleep mode status.
 * @param enabled New sleep mode enabled status
 * @see getSleepEnabled()
 * @see MPU6050_RA_PWR_MGMT_1
 * @see MPU6050_PWR1_SLEEP_BIT
 */
esp_err_t mpu6050_set_sleep_enable(mpu6050_handle_t sensor, bool enabled);

/** Set clock source setting.
 * An internal 8MHz oscillator, gyroscope based clock, or external sources can
 * be selected as the MPU-60X0 clock source. When the internal 8 MHz oscillator
 * or an external source is chosen as the clock source, the MPU-60X0 can operate
 * in low power modes with the gyroscopes disabled.
 *
 * Upon power up, the MPU-60X0 clock source defaults to the internal oscillator.
 * However, it is highly recommended that the device be configured to use one of
 * the gyroscopes (or an external clock source) as the clock reference for
 * improved stability. The clock source can be selected according to the following table:
 *
 * <pre>
 * CLK_SEL | Clock Source
 * --------+--------------------------------------
 * 0       | Internal oscillator
 * 1       | PLL with X Gyro reference
 * 2       | PLL with Y Gyro reference
 * 3       | PLL with Z Gyro reference
 * 4       | PLL with external 32.768kHz reference
 * 5       | PLL with external 19.2MHz reference
 * 6       | Reserved
 * 7       | Stops the clock and keeps the timing generator in reset
 * </pre>
 *
 * @param source New clock source setting
 * @see getClockSource()
 * @see MPU6050_RA_PWR_MGMT_1
 * @see MPU6050_PWR1_CLKSEL_BIT
 * @see MPU6050_PWR1_CLKSEL_LENGTH
 */
esp_err_t mpu6050_get_clock_source(mpu6050_handle_t sensor, uint8_t *buffer);

/** Get clock source setting.
 * @return Current clock source setting
 * @see MPU6050_RA_PWR_MGMT_1
 * @see MPU6050_PWR1_CLKSEL_BIT
 * @see MPU6050_PWR1_CLKSEL_LENGTH
 */
esp_err_t mpu6050_set_clock_source(mpu6050_handle_t sensor, uint8_t buffer);

/** Get full-scale accelerometer range.
 * The FS_SEL parameter allows setting the full-scale range of the accelerometer
 * sensors, as described in the table below.
 *
 * <pre>
 * 0 = +/- 2g
 * 1 = +/- 4g
 * 2 = +/- 8g
 * 3 = +/- 16g
 * </pre>
 *
 * @return Current full-scale accelerometer range setting
 * @see MPU6050_ACCEL_FS_2
 * @see MPU6050_RA_ACCEL_CONFIG
 * @see MPU6050_ACONFIG_AFS_SEL_BIT
 * @see MPU6050_ACONFIG_AFS_SEL_LENGTH
 */
esp_err_t mpu6050_get_full_scale_acce_range_id(mpu6050_handle_t sensor, uint8_t *buffer);

/** Get full-scale accelerometer G per LSB.
 *
 * @return float of current full-scale accelerometer setting as G per LSB
 * @see MPU6050_ACCEL_FS_2
 * @see MPU6050_RA_ACCEL_CONFIG
 * @see MPU6050_ACONFIG_AFS_SEL_BIT
 * @see MPU6050_ACONFIG_AFS_SEL_LENGTH
 */
esp_err_t mpu6050_get_full_scale_acce_range_gpl(mpu6050_handle_t sensor, float *buffer);

/** Get full-scale gyroscope range id.
 * The FS_SEL parameter allows setting the full-scale range of the gyro sensors,
 * as described in the table below.
 *
 * <pre>
 * 0 = +/- 250 degrees/sec
 * 1 = +/- 500 degrees/sec
 * 2 = +/- 1000 degrees/sec
 * 3 = +/- 2000 degrees/sec
 * </pre>
 *
 * @return Current full-scale gyroscope range setting
 * @see MPU6050_GYRO_FS_250
 * @see MPU6050_RA_GYRO_CONFIG
 * @see MPU6050_GCONFIG_FS_SEL_BIT
 * @see MPU6050_GCONFIG_FS_SEL_LENGTH
 */
esp_err_t mpu6050_get_full_scale_gyro_range_id(mpu6050_handle_t sensor, uint8_t *buffer);

/** Get full-scale gyroscope degrees per LSB.
 *
 * @return float of current full-scale gyroscope setting as degrees per LSB
 * @see MPU6050_GYRO_FS_250
 * @see MPU6050_RA_GYRO_CONFIG
 * @see MPU6050_GCONFIG_FS_SEL_BIT
 * @see MPU6050_GCONFIG_FS_SEL_LENGTH
 */
esp_err_t mpu6050_get_full_scale_gyro_range_dpl(mpu6050_handle_t sensor, float *buffer);

/** Get accelerometer offsets
 *
 */
esp_err_t mpu6050_get_x_acce_offset(mpu6050_handle_t sensor, int16_t *buffer);
esp_err_t mpu6050_get_y_acce_offset(mpu6050_handle_t sensor, int16_t *buffer);
esp_err_t mpu6050_get_z_acce_offset(mpu6050_handle_t sensor, int16_t *buffer);

/** Set accelerometer offsets
 *
 */
esp_err_t mpu6050_set_x_acce_offset(mpu6050_handle_t sensor, int16_t buffer);
esp_err_t mpu6050_set_y_acce_offset(mpu6050_handle_t sensor, int16_t buffer);
esp_err_t mpu6050_set_z_acce_offset(mpu6050_handle_t sensor, int16_t buffer);

/** Get gyroscope offsets
 *
 */
esp_err_t mpu6050_get_x_gyro_offset(mpu6050_handle_t sensor, int8_t *buffer);
esp_err_t mpu6050_get_y_gyro_offset(mpu6050_handle_t sensor, int8_t *buffer);
esp_err_t mpu6050_get_z_gyro_offset(mpu6050_handle_t sensor, int8_t *buffer);

/** Set gyroscope offsets
 *
 */
esp_err_t mpu6050_set_x_gyro_offset(mpu6050_handle_t sensor, int8_t buffer);
esp_err_t mpu6050_set_y_gyro_offset(mpu6050_handle_t sensor, int8_t buffer);
esp_err_t mpu6050_set_z_gyro_offset(mpu6050_handle_t sensor, int8_t buffer);

/** Reset the FIFO.
 * This bit resets the FIFO buffer when set to 1 while FIFO_EN equals 0. This
 * bit automatically clears to 0 after the reset has been triggered.
 * @see MPU6050_RA_USER_CTRL
 * @see MPU6050_USERCTRL_FIFO_RESET_BIT
 */
esp_err_t mpu6050_reset_fifo(mpu6050_handle_t sensor);

/** Reset DMP.
 * This bit resets the DMP, not much is known about DMP functionality of MPU's
 * DMP.
 * @see MPU6050_RA_USER_CTRL
 * @see MPU6050_USERCTRL_DMP_RESET_BIT
 */
esp_err_t mpu6050_reset_dmp(mpu6050_handle_t sensor);

/**
 * @brief Set accelerometer full scale range
 *
 * @param sensor object handle of mpu6050
 * @param acce_fs accelerometer full scale range
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_FAIL Fail
 */
esp_err_t mpu6050_set_acce_fs(mpu6050_handle_t sensor, mpu6050_acce_fs_t acce_fs);

/**
 * @brief Set gyroscope full scale range
 *
 * @param sensor object handle of mpu6050
 * @param gyro_fs gyroscope full scale range
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_FAIL Fail
 */
esp_err_t mpu6050_set_gyro_fs(mpu6050_handle_t sensor, mpu6050_gyro_fs_t gyro_fs);

/**
 * @brief Get accelerometer full scale range
 *
 * @param sensor object handle of mpu6050
 * @param acce_fs accelerometer full scale range
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_FAIL Fail
 */
esp_err_t mpu6050_get_acce_fs(mpu6050_handle_t sensor, mpu6050_acce_fs_t *acce_fs);

/**
 * @brief Get gyroscope full scale range
 *
 * @param sensor object handle of mpu6050
 * @param gyro_fs gyroscope full scale range
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_FAIL Fail
 */
esp_err_t mpu6050_get_gyro_fs(mpu6050_handle_t sensor, mpu6050_gyro_fs_t *gyro_fs);

/**
 * @brief Get accelerometer sensitivity
 *
 * @param sensor object handle of mpu6050
 * @param acce_sensitivity accelerometer sensitivity
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_FAIL Fail
 */
esp_err_t mpu6050_get_acce_sensitivity(mpu6050_handle_t sensor, float *acce_sensitivity);

/**
 * @brief Get gyroscope sensitivity
 *
 * @param sensor object handle of mpu6050
 * @param gyro_sensitivity gyroscope sensitivity
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_FAIL Fail
 */
esp_err_t mpu6050_get_gyro_sensitivity(mpu6050_handle_t sensor, float *gyro_sensitivity);

/**
 * @brief Read raw accelerometer measurements
 *
 * @param sensor object handle of mpu6050
 * @param acce_value raw accelerometer measurements
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_FAIL Fail
 */
esp_err_t mpu6050_get_raw_acce(mpu6050_handle_t sensor, mpu6050_raw_acce_value_t *raw_acce_value);

/**
 * @brief Read raw gyroscope measurements
 *
 * @param sensor object handle of mpu6050
 * @param gyro_value raw gyroscope measurements
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_FAIL Fail
 */
esp_err_t mpu6050_get_raw_gyro(mpu6050_handle_t sensor, mpu6050_raw_gyro_value_t *raw_gyro_value);

/**
 * @brief Read accelerometer measurements
 *
 * @param sensor object handle of mpu6050
 * @param acce_value accelerometer measurements
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_FAIL Fail
 */
esp_err_t mpu6050_get_acce(mpu6050_handle_t sensor, mpu6050_acce_value_t *acce_value);

/**
 * @brief Read gyro values
 *
 * @param sensor object handle of mpu6050
 * @param gyro_value gyroscope measurements
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_FAIL Fail
 */
esp_err_t mpu6050_get_gyro(mpu6050_handle_t sensor, mpu6050_gyro_value_t *gyro_value);

/**
 * @brief use complimentory filter to caculate roll and pitch
 *
 * @param acce_value accelerometer measurements
 * @param gyro_value gyroscope measurements
 * @param complimentary_angle complimentary angle
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_FAIL Fail
 */
esp_err_t mpu6050_complimentory_filter(mpu6050_handle_t sensor, mpu6050_acce_value_t *acce_value,
				       mpu6050_gyro_value_t *gyro_value,
				       complimentary_angle_t *complimentary_angle);

/**
 * @brief print the current settings of the MPU6050
 */
void mpu6050_check_settings(mpu6050_handle_t);

esp_err_t mpu6050_get_int_dmp_enabled(mpu6050_handle_t sensor, uint8_t *buffer);
esp_err_t mpu6050_set_int_dmp_enabled(mpu6050_handle_t sensor, bool enabled);
esp_err_t mpu6050_get_int_dmp_status(mpu6050_handle_t sensor, uint8_t *buffer);

esp_err_t mpu6050_get_int_dmp_int5_status(mpu6050_handle_t sensor, uint8_t *buffer);
esp_err_t mpu6050_get_int_dmp_int4_status(mpu6050_handle_t sensor, uint8_t *buffer);
esp_err_t mpu6050_get_int_dmp_int3_status(mpu6050_handle_t sensor, uint8_t *buffer);
esp_err_t mpu6050_get_int_dmp_int2_status(mpu6050_handle_t sensor, uint8_t *buffer);
esp_err_t mpu6050_get_int_dmp_int1_status(mpu6050_handle_t sensor, uint8_t *buffer);
esp_err_t mpu6050_get_int_dmp_int0_status(mpu6050_handle_t sensor, uint8_t *buffer);
esp_err_t mpu6050_get_int_dmp_status(mpu6050_handle_t sensor, uint8_t *buffer);
esp_err_t mpu6050_get_dmp_enabled(mpu6050_handle_t sensor, uint8_t *buffer);
esp_err_t mpu6050_set_dmp_enabled(mpu6050_handle_t sensor, bool enabled);

/** Get X-axis negative motion detection interrupt status.
 * @return Motion detection status
 * @see MPU6050_RA_MOT_DETECT_STATUS
 * @see MPU6050_MOTION_MOT_XNEG_BIT
 */
esp_err_t mpu6050_get_x_neg_motion_detected(mpu6050_handle_t sensor, uint8_t *buffer);

/** Get X-axis positive motion detection interrupt status.
 * @return Motion detection status
 * @see MPU6050_RA_MOT_DETECT_STATUS
 * @see MPU6050_MOTION_MOT_XPOS_BIT
 */
esp_err_t mpu6050_get_x_pos_motion_detected(mpu6050_handle_t sensor, uint8_t *buffer);

/** Get Y-axis negative motion detection interrupt status.
 * @return Motion detection status
 * @see MPU6050_RA_MOT_DETECT_STATUS
 * @see MPU6050_MOTION_MOT_YNEG_BIT
 */
esp_err_t mpu6050_get_y_neg_motion_detected(mpu6050_handle_t sensor, uint8_t *buffer);

/** Get Y-axis positive motion detection interrupt status.
 * @return Motion detection status
 * @see MPU6050_RA_MOT_DETECT_STATUS
 * @see MPU6050_MOTION_MOT_YPOS_BIT
 */
esp_err_t mpu6050_get_y_pos_motion_detected(mpu6050_handle_t sensor, uint8_t *buffer);

/** Get Z-axis negative motion detection interrupt status.
 * @return Motion detection status
 * @see MPU6050_RA_MOT_DETECT_STATUS
 * @see MPU6050_MOTION_MOT_ZNEG_BIT
 */
esp_err_t mpu6050_get_z_neg_motion_detected(mpu6050_handle_t sensor, uint8_t *buffer);

/** Get Z-axis positive motion detection interrupt status.
 * @return Motion detection status
 * @see MPU6050_RA_MOT_DETECT_STATUS
 * @see MPU6050_MOTION_MOT_ZPOS_BIT
 */
esp_err_t mpu6050_get_z_pos_motion_detected(mpu6050_handle_t sensor, uint8_t *buffer);

/** Set digital low-pass filter configuration.
 * @param mode New DLFP configuration setting
 * @see getDLPFBandwidth()
 * @see MPU6050_DLPF_BW_256
 * @see MPU6050_RA_CONFIG
 * @see MPU6050_CFG_DLPF_CFG_BIT
 * @see MPU6050_CFG_DLPF_CFG_LENGTH
 */
esp_err_t mpu6050_set_dlpf_mode(mpu6050_handle_t sensor, bool enabled);

/** Get digital low-pass filter configuration.
 * The DLPF_CFG parameter sets the digital low pass filter configuration. It
 * also determines the internal sampling rate used by the device as shown in
 * the table below.
 *
 * Note: The accelerometer output rate is 1kHz. This means that for a Sample
 * Rate greater than 1kHz, the same accelerometer sample may be output to the
 * FIFO, DMP, and sensor registers more than once.
 *
 * <pre>
 *          |   ACCELEROMETER    |           GYROSCOPE
 * DLPF_CFG | Bandwidth | Delay  | Bandwidth | Delay  | Sample Rate
 * ---------+-----------+--------+-----------+--------+-------------
 * 0        | 260Hz     | 0ms    | 256Hz     | 0.98ms | 8kHz
 * 1        | 184Hz     | 2.0ms  | 188Hz     | 1.9ms  | 1kHz
 * 2        | 94Hz      | 3.0ms  | 98Hz      | 2.8ms  | 1kHz
 * 3        | 44Hz      | 4.9ms  | 42Hz      | 4.8ms  | 1kHz
 * 4        | 21Hz      | 8.5ms  | 20Hz      | 8.3ms  | 1kHz
 * 5        | 10Hz      | 13.8ms | 10Hz      | 13.4ms | 1kHz
 * 6        | 5Hz       | 19.0ms | 5Hz       | 18.6ms | 1kHz
 * 7        |   -- Reserved --   |   -- Reserved --   | Reserved
 * </pre>
 *
 * @return DLFP configuration
 * @see MPU6050_RA_CONFIG
 * @see MPU6050_CFG_DLPF_CFG_BIT
 * @see MPU6050_CFG_DLPF_CFG_LENGTH
 */
esp_err_t mpu6050_get_dlpf_mode(mpu6050_handle_t sensor, uint8_t *buffer);

/** Set gyroscope sample rate divider.
 * @param rate New sample rate divider
 * @see getRate()
 * @see MPU6050_RA_SMPLRT_DIV
 */
esp_err_t mpu6050_set_sample_rate(mpu6050_handle_t sensor, uint8_t buffer);

/** Get gyroscope output rate divider.
 * The sensor register output, FIFO output, DMP sampling, Motion detection, Zero
 * Motion detection, and Free Fall detection are all based on the Sample Rate.
 * The Sample Rate is generated by dividing the gyroscope output rate by
 * SMPLRT_DIV:
 *
 * Sample Rate = Gyroscope Output Rate / (1 + SMPLRT_DIV)
 *
 * where Gyroscope Output Rate = 8kHz when the DLPF is disabled (DLPF_CFG = 0 or
 * 7), and 1kHz when the DLPF is enabled (see Register 26).
 *
 * Note: The accelerometer output rate is 1kHz. This means that for a Sample
 * Rate greater than 1kHz, the same accelerometer sample may be output to the
 * FIFO, DMP, and sensor registers more than once.
 *
 * For a diagram of the gyroscope and accelerometer signal paths, see Section 8
 * of the MPU-6000/MPU-6050 Product Specification document.
 *
 * @return Current sample rate
 * @see MPU6050_RA_SMPLRT_DIV
 */
esp_err_t mpu6050_get_sample_rate(mpu6050_handle_t sensor, uint8_t *buffer);

/** Get temperature FIFO enabled value.
 * When set to 1, this bit enables TEMP_OUT_H and TEMP_OUT_L (Registers 65 and
 * 66) to be written into the FIFO buffer.
 * @return Current temperature FIFO enabled value
 * @see MPU6050_RA_FIFO_EN
 */
esp_err_t mpu6050_get_temp_fifo_enabled(mpu6050_handle_t sensor, uint8_t *buffer);

/** Set temperature FIFO enabled value.
 * @param enabled New temperature FIFO enabled value
 * @see getTempFIFOEnabled()
 * @see MPU6050_RA_FIFO_EN
 */
esp_err_t mpu6050_set_temp_fifo_enabled(mpu6050_handle_t sensor, bool enabled);

/** Get gyroscope X-axis FIFO enabled value.
 * When set to 1, this bit enables GYRO_XOUT_H and GYRO_XOUT_L (Registers 67 and
 * 68) to be written into the FIFO buffer.
 * @return Current gyroscope X-axis FIFO enabled value
 * @see MPU6050_RA_FIFO_EN
 */
esp_err_t mpu6050_get_x_gyro_fifo_enabled(mpu6050_handle_t sensor, uint8_t *buffer);

/** Set gyroscope X-axis FIFO enabled value.
 * @param enabled New gyroscope X-axis FIFO enabled value
 * @see getXGyroFIFOEnabled()
 * @see MPU6050_RA_FIFO_EN
 */
esp_err_t mpu6050_set_x_gyro_fifo_enabled(mpu6050_handle_t sensor, bool enabled);

/** Get gyroscope Y-axis FIFO enabled value.
 * When set to 1, this bit enables GYRO_YOUT_H and GYRO_YOUT_L (Registers 69 and
 * 70) to be written into the FIFO buffer.
 * @return Current gyroscope Y-axis FIFO enabled value
 * @see MPU6050_RA_FIFO_EN
 */
esp_err_t mpu6050_get_y_gyro_fifo_enabled(mpu6050_handle_t sensor, uint8_t *buffer);

/** Set gyroscope Y-axis FIFO enabled value.
 * @param enabled New gyroscope Y-axis FIFO enabled value
 * @see getYGyroFIFOEnabled()
 * @see MPU6050_RA_FIFO_EN
 */
esp_err_t mpu6050_set_y_gyro_fifo_enabled(mpu6050_handle_t sensor, bool enabled);

/** Get gyroscope Z-axis FIFO enabled value.
 * When set to 1, this bit enables GYRO_ZOUT_H and GYRO_ZOUT_L (Registers 71 and
 * 72) to be written into the FIFO buffer.
 * @return Current gyroscope Z-axis FIFO enabled value
 * @see MPU6050_RA_FIFO_EN
 */
esp_err_t mpu6050_get_z_gyro_fifo_enabled(mpu6050_handle_t sensor, uint8_t *buffer);

/** Set gyroscope Z-axis FIFO enabled value.
 * @param enabled New gyroscope Z-axis FIFO enabled value
 * @see getZGyroFIFOEnabled()
 * @see MPU6050_RA_FIFO_EN
 */
esp_err_t mpu6050_set_z_gyro_fifo_enabled(mpu6050_handle_t sensor, bool enabled);

/** Get accelerometer FIFO enabled value.
 * When set to 1, this bit enables ACCEL_XOUT_H, ACCEL_XOUT_L, ACCEL_YOUT_H,
 * ACCEL_YOUT_L, ACCEL_ZOUT_H, and ACCEL_ZOUT_L (Registers 59 to 64) to be
 * written into the FIFO buffer.
 * @return Current accelerometer FIFO enabled value
 * @see MPU6050_RA_FIFO_EN
 */
esp_err_t mpu6050_get_acce_fifo_enabled(mpu6050_handle_t sensor, uint8_t *buffer);

/** Set accelerometer FIFO enabled value.
 * @param enabled New accelerometer FIFO enabled value
 * @see getAccelFIFOEnabled()
 * @see MPU6050_RA_FIFO_EN
 */
esp_err_t mpu6050_set_acce_fifo_enabled(mpu6050_handle_t sensor, bool enabled);

/**
 *  @brief      Load and verify DMP image.
 *  @param[in]  length      Length of DMP image.
 *  @param[in]  firmware    DMP code.
 *  @param[in]  start_addr  Starting address of DMP code memory.
 *  @param[in]  sample_rate Fixed sampling rate used when DMP is enabled.
 */
esp_err_t mpu6050_load_firmware(mpu6050_handle_t sensor, unsigned short length,
				const uint8_t *firmware, unsigned short start_addr,
				unsigned short sample_rate);

/** Set interrupt latch mode.
 * @param latch New latch mode (0=50us-pulse, 1=latch-until-int-cleared)
 * @see getInterruptLatch()
 * @see MPU6050_RA_INT_PIN_CFG
 * @see MPU6050_INTCFG_LATCH_INT_EN_BIT
 */
esp_err_t mpu6050_set_interrupt_latch(mpu6050_handle_t sensor, uint8_t buffer);

/** Get interrupt latch mode.
 * Will be set 0 for 50us-pulse, 1 for latch-until-int-cleared.
 * @return Current latch mode (0=50us-pulse, 1=latch-until-int-cleared)
 * @see MPU6050_RA_INT_PIN_CFG
 * @see MPU6050_INTCFG_LATCH_INT_EN_BIT
 */
esp_err_t mpu6050_get_interrupt_latch(mpu6050_handle_t sensor, uint8_t *buffer);

/** Set interrupt logic level mode.
 * @param mode New interrupt mode (0=active-high, 1=active-low)
 * @see getInterruptMode()
 * @see MPU6050_RA_INT_PIN_CFG
 * @see MPU6050_INTCFG_INT_LEVEL_BIT
 */
esp_err_t mpu6050_set_interrupt_mode(mpu6050_handle_t sensor, uint8_t buffer);

/** Get interrupt logic level mode.
 * Will be set 0 for active-high, 1 for active-low.
 * @return Current interrupt mode (0=active-high, 1=active-low)
 * @see MPU6050_RA_INT_PIN_CFG
 * @see MPU6050_INTCFG_INT_LEVEL_BIT
 */
esp_err_t mpu6050_get_interrupt_mode(mpu6050_handle_t sensor, uint8_t *buffer);

#ifdef __cplusplus
}
#endif

#endif
