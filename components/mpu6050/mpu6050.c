// Copyright 2015-2016 Espressif Systems (Shanghai) PTE LTD
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include <math.h>
#include <string.h>
#include "driver/i2c.h"
#include "esp_system.h"
#include "esp_log.h"
#include "mpu6050.h"

#define min(a, b) ((a < b) ? a : b)

#define WRITE_BIT     I2C_MASTER_WRITE /*!< I2C master write */
#define READ_BIT      I2C_MASTER_READ /*!< I2C master read */
#define ACK_CHECK_EN  0x1 /*!< I2C master will check ack from slave*/
#define ACK_CHECK_DIS 0x0 /*!< I2C master will not check ack from slave */
#define ACK_VAL	      0x0 /*!< I2C ack value */
#define NACK_VAL      0x1 /*!< I2C nack value */

#define ALPHA	   0.99 /*!< Weight for gyroscope */
#define RAD_TO_DEG 57.27272727 /*!< Radians to degrees */

typedef struct {
	i2c_bus_device_handle_t i2c_dev;
	uint8_t dev_addr;
	uint32_t counter;
	float dt; /*!< delay time between twice measurement, dt should be small (ms level) */
	struct timeval *timer;
} mpu6050_dev_t;

mpu6050_handle_t mpu6050_create(i2c_bus_handle_t bus, uint8_t dev_addr)
{
	if (bus == NULL) {
		return NULL;
	}

	mpu6050_dev_t *sens = (mpu6050_dev_t *)calloc(1, sizeof(mpu6050_dev_t));
	sens->i2c_dev = i2c_bus_device_create(bus, dev_addr, i2c_bus_get_current_clk_speed(bus));
	if (sens->i2c_dev == NULL) {
		free(sens);
		return NULL;
	}
	sens->dev_addr = dev_addr;
	sens->counter = 0;
	sens->dt = 0;
	sens->timer = (struct timeval *)calloc(1, sizeof(struct timeval));
	return (mpu6050_handle_t)sens;
}

esp_err_t mpu6050_delete(mpu6050_handle_t *sensor)
{
	if (*sensor == NULL) {
		return ESP_OK;
	}

	mpu6050_dev_t *sens = (mpu6050_dev_t *)(*sensor);
	i2c_bus_device_delete(&sens->i2c_dev);
	free(sens->timer);
	free(sens);
	*sensor = NULL;
	return ESP_OK;
}

esp_err_t mpu6050_get_deviceid(mpu6050_handle_t sensor, uint8_t *deviceid)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	uint8_t tmp;
	ret = i2c_bus_read_byte(sens->i2c_dev, MPU6050_WHO_AM_I, &tmp);
	*deviceid = tmp;
	return ret;
}

esp_err_t mpu6050_wake_up(mpu6050_handle_t sensor)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	uint8_t tmp;
	ret = i2c_bus_read_byte(sens->i2c_dev, MPU6050_PWR_MGMT_1, &tmp);

	if (ret != ESP_OK) {
		return ret;
	}

	tmp &= (~BIT6);
	ret = i2c_bus_write_byte(sens->i2c_dev, MPU6050_PWR_MGMT_1, tmp);
	return ret;
}

esp_err_t mpu6050_sleep(mpu6050_handle_t sensor)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	uint8_t tmp;
	ret = i2c_bus_read_byte(sens->i2c_dev, MPU6050_PWR_MGMT_1, &tmp);

	if (ret != ESP_OK) {
		return ret;
	}

	tmp |= BIT6;
	ret = i2c_bus_write_byte(sens->i2c_dev, MPU6050_PWR_MGMT_1, tmp);
	return ret;
}

esp_err_t mpu6050_reset_fifo(mpu6050_handle_t sensor)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret = i2c_bus_write_bit(sens->i2c_dev, MPU6050_USER_CTRL,
					  MPU6050_USERCTRL_FIFO_RESET_BIT, true);
	return ret;
}

esp_err_t mpu6050_reset_dmp(mpu6050_handle_t sensor)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret = i2c_bus_write_bit(sens->i2c_dev, MPU6050_USER_CTRL,
					  MPU6050_USERCTRL_DMP_RESET_BIT, true);
	return ret;
}

esp_err_t mpu6050_get_int_zero_motion_enabled(mpu6050_handle_t sensor, uint8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_read_bit(sens->i2c_dev, MPU6050_INT_ENABLE, MPU6050_INTERRUPT_ZMOT_BIT,
			       buffer);
	return ret;
}

esp_err_t mpu6050_set_int_zero_motion_enabled(mpu6050_handle_t sensor, bool enabled)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_write_bit(sens->i2c_dev, MPU6050_INT_ENABLE, MPU6050_INTERRUPT_ZMOT_BIT,
				enabled);
	return ret;
}

esp_err_t mpu6050_get_int_motion_enabled(mpu6050_handle_t sensor, uint8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_read_bit(sens->i2c_dev, MPU6050_INT_ENABLE, MPU6050_INTERRUPT_MOT_BIT,
			       buffer);
	return ret;
}

esp_err_t mpu6050_set_int_motion_enabled(mpu6050_handle_t sensor, bool enabled)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_write_bit(sens->i2c_dev, MPU6050_INT_ENABLE, MPU6050_INTERRUPT_MOT_BIT,
				enabled);
	return ret;
}

esp_err_t mpu6050_get_int_motion_status(mpu6050_handle_t sensor, uint8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_read_bit(sens->i2c_dev, MPU6050_INT_STATUS, MPU6050_INTERRUPT_MOT_BIT,
			       buffer);
	return ret;
}

esp_err_t mpu6050_get_int_zero_motion_status(mpu6050_handle_t sensor, uint8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_read_bit(sens->i2c_dev, MPU6050_INT_STATUS, MPU6050_INTERRUPT_ZMOT_BIT,
			       buffer);
	return ret;
}

esp_err_t mpu6050_get_int_status(mpu6050_handle_t sensor, uint8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_read_byte(sens->i2c_dev, MPU6050_INT_STATUS, buffer);
	return ret;
}

esp_err_t mpu6050_get_zero_motion_detection_threshold(mpu6050_handle_t sensor, uint8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_read_byte(sens->i2c_dev, MPU6050_ZRMOT_THR, buffer);
	return ret;
}

esp_err_t mpu6050_set_zero_motion_detection_threshold(mpu6050_handle_t sensor, uint8_t buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_write_byte(sens->i2c_dev, MPU6050_ZRMOT_THR, buffer);
	return ret;
}

esp_err_t mpu6050_get_zero_motion_detection_duration(mpu6050_handle_t sensor, uint8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_read_byte(sens->i2c_dev, MPU6050_ZRMOT_DUR, buffer);
	return ret;
}

esp_err_t mpu6050_set_zero_motion_detection_duration(mpu6050_handle_t sensor, uint8_t buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_write_byte(sens->i2c_dev, MPU6050_ZRMOT_DUR, buffer);
	return ret;
}

esp_err_t mpu6050_get_motion_detection_threshold(mpu6050_handle_t sensor, uint8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_read_byte(sens->i2c_dev, MPU6050_MOT_THR, buffer);
	return ret;
}

esp_err_t mpu6050_set_motion_detection_threshold(mpu6050_handle_t sensor, uint8_t buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_write_byte(sens->i2c_dev, MPU6050_MOT_THR, buffer);
	return ret;
}

esp_err_t mpu6050_get_motion_detection_duration(mpu6050_handle_t sensor, uint8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_read_byte(sens->i2c_dev, MPU6050_MOT_DUR, buffer);
	return ret;
}

esp_err_t mpu6050_set_motion_detection_duration(mpu6050_handle_t sensor, uint8_t buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_write_byte(sens->i2c_dev, MPU6050_MOT_DUR, buffer);
	return ret;
}

esp_err_t mpu6050_get_dhpf_mode(mpu6050_handle_t sensor, uint8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_read_bits(sens->i2c_dev, MPU6050_ACCEL_CONFIG, MPU6050_ACONFIG_ACCEL_HPF_BIT,
				MPU6050_ACONFIG_ACCEL_HPF_LENGTH, buffer);
	return ret;
}

esp_err_t mpu6050_set_dhpf_mode(mpu6050_handle_t sensor, uint8_t buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_write_bits(sens->i2c_dev, MPU6050_ACCEL_CONFIG, MPU6050_ACONFIG_ACCEL_HPF_BIT,
				 MPU6050_ACONFIG_ACCEL_HPF_LENGTH, buffer);
	return ret;
}

esp_err_t mpu6050_get_acce_poweron_delay(mpu6050_handle_t sensor, uint8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_read_bits(sens->i2c_dev, MPU6050_MOT_DETECT_CTRL,
				MPU6050_DETECT_ACCEL_ON_DELAY_BIT,
				MPU6050_DETECT_ACCEL_ON_DELAY_LENGTH, buffer);
	return ret;
}

esp_err_t mpu6050_set_acce_poweron_delay(mpu6050_handle_t sensor, uint8_t buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_write_bits(sens->i2c_dev, MPU6050_MOT_DETECT_CTRL,
				 MPU6050_DETECT_ACCEL_ON_DELAY_BIT,
				 MPU6050_DETECT_ACCEL_ON_DELAY_LENGTH, buffer);
	return ret;
}

esp_err_t mpu6050_get_int_freefall_enable(mpu6050_handle_t sensor, uint8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_read_bit(sens->i2c_dev, MPU6050_INT_ENABLE, MPU6050_INTERRUPT_FF_BIT, buffer);
	return ret;
}

esp_err_t mpu6050_set_int_freefall_enable(mpu6050_handle_t sensor, bool enabled)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_write_bit(sens->i2c_dev, MPU6050_INT_ENABLE, MPU6050_INTERRUPT_FF_BIT,
				enabled);
	return ret;
}

esp_err_t mpu6050_get_sleep_enable(mpu6050_handle_t sensor, uint8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_read_bit(sens->i2c_dev, MPU6050_PWR_MGMT_1, MPU6050_PWR1_SLEEP_BIT, buffer);
	return ret;
}
esp_err_t mpu6050_set_sleep_enable(mpu6050_handle_t sensor, bool enabled)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_write_bit(sens->i2c_dev, MPU6050_PWR_MGMT_1, MPU6050_PWR1_SLEEP_BIT, enabled);
	return ret;
}

esp_err_t mpu6050_set_acce_fs(mpu6050_handle_t sensor, mpu6050_acce_fs_t acce_fs)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	uint8_t tmp;
	ret = i2c_bus_read_byte(sens->i2c_dev, MPU6050_ACCEL_CONFIG, &tmp);

	if (ret != ESP_OK) {
		return ret;
	}

	tmp &= (~BIT3);
	tmp &= (~BIT4);
	tmp |= (acce_fs << 3);
	ret = i2c_bus_write_byte(sens->i2c_dev, MPU6050_ACCEL_CONFIG, tmp);
	return ret;
}

esp_err_t mpu6050_set_gyro_fs(mpu6050_handle_t sensor, mpu6050_gyro_fs_t gyro_fs)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	uint8_t tmp;
	ret = i2c_bus_read_byte(sens->i2c_dev, MPU6050_GYRO_CONFIG, &tmp);

	if (ret != ESP_OK) {
		return ret;
	}

	tmp &= (~BIT3);
	tmp &= (~BIT4);
	tmp |= (gyro_fs << 3);
	ret = i2c_bus_write_byte(sens->i2c_dev, MPU6050_GYRO_CONFIG, tmp);
	return ret;
}

esp_err_t mpu6050_get_acce_fs(mpu6050_handle_t sensor, mpu6050_acce_fs_t *acce_fs)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	uint8_t tmp;
	ret = i2c_bus_read_byte(sens->i2c_dev, MPU6050_ACCEL_CONFIG, &tmp);
	tmp = (tmp >> 3) & 0x03;
	*acce_fs = tmp;
	return ret;
}

esp_err_t mpu6050_get_gyro_fs(mpu6050_handle_t sensor, mpu6050_gyro_fs_t *gyro_fs)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	uint8_t tmp;
	ret = i2c_bus_read_byte(sens->i2c_dev, MPU6050_GYRO_CONFIG, &tmp);
	tmp = (tmp >> 3) & 0x03;
	*gyro_fs = tmp;
	return ret;
}

esp_err_t mpu6050_get_acce_sensitivity(mpu6050_handle_t sensor, float *acce_sensitivity)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	uint8_t acce_fs;
	ret = i2c_bus_read_byte(sens->i2c_dev, MPU6050_ACCEL_CONFIG, &acce_fs);
	acce_fs = (acce_fs >> 3) & 0x03;
	switch (acce_fs) {
	case ACCE_FS_2G:
		*acce_sensitivity = 16384;
		break;
	case ACCE_FS_4G:
		*acce_sensitivity = 8192;
		break;
	case ACCE_FS_8G:
		*acce_sensitivity = 4096;
		break;
	case ACCE_FS_16G:
		*acce_sensitivity = 2048;
		break;
	default:
		break;
	}
	return ret;
}

esp_err_t mpu6050_get_gyro_sensitivity(mpu6050_handle_t sensor, float *gyro_sensitivity)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	uint8_t gyro_fs;
	ret = i2c_bus_read_byte(sens->i2c_dev, MPU6050_GYRO_CONFIG, &gyro_fs);
	gyro_fs = (gyro_fs >> 3) & 0x03;
	switch (gyro_fs) {
	case GYRO_FS_250DPS:
		*gyro_sensitivity = 131;
		break;
	case GYRO_FS_500DPS:
		*gyro_sensitivity = 65.5;
		break;
	case GYRO_FS_1000DPS:
		*gyro_sensitivity = 32.8;
		break;
	case GYRO_FS_2000DPS:
		*gyro_sensitivity = 16.4;
		break;
	default:
		break;
	}
	return ret;
}

esp_err_t mpu6050_get_raw_acce(mpu6050_handle_t sensor, mpu6050_raw_acce_value_t *raw_acce_value)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	uint8_t data_rd[6] = { 0 };
	esp_err_t ret = i2c_bus_read_bytes(sens->i2c_dev, MPU6050_ACCEL_XOUT_H, 6, data_rd);
	raw_acce_value->raw_acce_x = (int16_t)((data_rd[0] << 8) + (data_rd[1]));
	raw_acce_value->raw_acce_y = (int16_t)((data_rd[2] << 8) + (data_rd[3]));
	raw_acce_value->raw_acce_z = (int16_t)((data_rd[4] << 8) + (data_rd[5]));
	return ret;
}

esp_err_t mpu6050_get_raw_gyro(mpu6050_handle_t sensor, mpu6050_raw_gyro_value_t *raw_gyro_value)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	uint8_t data_rd[6] = { 0 };
	esp_err_t ret = i2c_bus_read_bytes(sens->i2c_dev, MPU6050_GYRO_XOUT_H, 6, data_rd);
	raw_gyro_value->raw_gyro_x = (int16_t)((data_rd[0] << 8) + (data_rd[1]));
	raw_gyro_value->raw_gyro_y = (int16_t)((data_rd[2] << 8) + (data_rd[3]));
	raw_gyro_value->raw_gyro_z = (int16_t)((data_rd[4] << 8) + (data_rd[5]));
	return ret;
}

esp_err_t mpu6050_get_acce(mpu6050_handle_t sensor, mpu6050_acce_value_t *acce_value)
{
	esp_err_t ret;
	float acce_sensitivity;
	mpu6050_raw_acce_value_t raw_acce;
	ret = mpu6050_get_acce_sensitivity(sensor, &acce_sensitivity);

	if (ret != ESP_OK) {
		return ret;
	}

	ret = mpu6050_get_raw_acce(sensor, &raw_acce);

	if (ret != ESP_OK) {
		return ret;
	}

	acce_value->acce_x = raw_acce.raw_acce_x / acce_sensitivity;
	acce_value->acce_y = raw_acce.raw_acce_y / acce_sensitivity;
	acce_value->acce_z = raw_acce.raw_acce_z / acce_sensitivity;
	return ESP_OK;
}

esp_err_t mpu6050_get_gyro(mpu6050_handle_t sensor, mpu6050_gyro_value_t *gyro_value)
{
	esp_err_t ret;
	float gyro_sensitivity;
	mpu6050_raw_gyro_value_t raw_gyro;
	ret = mpu6050_get_gyro_sensitivity(sensor, &gyro_sensitivity);

	if (ret != ESP_OK) {
		return ret;
	}

	ret = mpu6050_get_raw_gyro(sensor, &raw_gyro);

	if (ret != ESP_OK) {
		return ret;
	}

	gyro_value->gyro_x = raw_gyro.raw_gyro_x / gyro_sensitivity;
	gyro_value->gyro_y = raw_gyro.raw_gyro_y / gyro_sensitivity;
	gyro_value->gyro_z = raw_gyro.raw_gyro_z / gyro_sensitivity;
	return ESP_OK;
}

esp_err_t mpu6050_complimentory_filter(mpu6050_handle_t sensor, mpu6050_acce_value_t *acce_value,
				       mpu6050_gyro_value_t *gyro_value,
				       complimentary_angle_t *complimentary_angle)
{
	float acce_angle[2];
	float gyro_angle[2];
	float gyro_rate[2];
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	sens->counter++;

	if (sens->counter == 1) {
		acce_angle[0] = (atan2(acce_value->acce_y, acce_value->acce_z) * RAD_TO_DEG);
		acce_angle[1] = (atan2(acce_value->acce_x, acce_value->acce_z) * RAD_TO_DEG);
		complimentary_angle->roll = acce_angle[0];
		complimentary_angle->pitch = acce_angle[1];
		gettimeofday(sens->timer, NULL);
		return ESP_OK;
	}

	struct timeval now, dt_t;
	gettimeofday(&now, NULL);
	timersub(&now, sens->timer, &dt_t);
	sens->dt = (float)(dt_t.tv_sec) + (float)dt_t.tv_usec / 1000000;
	gettimeofday(sens->timer, NULL);
	acce_angle[0] = (atan2(acce_value->acce_y, acce_value->acce_z) * RAD_TO_DEG);
	acce_angle[1] = (atan2(acce_value->acce_x, acce_value->acce_z) * RAD_TO_DEG);
	gyro_rate[0] = gyro_value->gyro_x;
	gyro_rate[1] = gyro_value->gyro_y;
	gyro_angle[0] = gyro_rate[0] * sens->dt;
	gyro_angle[1] = gyro_rate[1] * sens->dt;
	complimentary_angle->roll = (ALPHA * (complimentary_angle->roll + gyro_angle[0])) +
				    ((1 - ALPHA) * acce_angle[0]);
	complimentary_angle->pitch = (ALPHA * (complimentary_angle->pitch + gyro_angle[1])) +
				     ((1 - ALPHA) * acce_angle[1]);
	return ESP_OK;
}

esp_err_t mpu6050_get_x_acce_offset(mpu6050_handle_t sensor, int16_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	uint8_t *buffer_t = malloc(sizeof(uint8_t *) * 10);
	esp_err_t ret;
	ret = i2c_bus_read_reg8(sens->i2c_dev, MPU6050_XA_OFFS_H, 2, buffer_t);
	*buffer = (((int16_t)buffer_t[0]) << 8) | buffer_t[1];
	free(buffer_t);
	return ret;
}

esp_err_t mpu6050_set_x_acce_offset(mpu6050_handle_t sensor, int16_t buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_write_reg8(sens->i2c_dev, MPU6050_XA_OFFS_H, 2, (uint8_t *)&buffer);
	return ret;
}

esp_err_t mpu6050_get_y_acce_offset(mpu6050_handle_t sensor, int16_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	uint8_t *buffer_t = malloc(sizeof(uint8_t *) * 10);
	esp_err_t ret;
	ret = i2c_bus_read_reg8(sens->i2c_dev, MPU6050_YA_OFFS_H, 2, buffer_t);
	*buffer = (((int16_t)buffer_t[0]) << 8) | buffer_t[1];
	free(buffer_t);
	return ret;
}

esp_err_t mpu6050_set_y_acce_offset(mpu6050_handle_t sensor, int16_t buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_write_reg8(sens->i2c_dev, MPU6050_YA_OFFS_H, 2, (uint8_t *)&buffer);
	return ret;
}

esp_err_t mpu6050_get_z_acce_offset(mpu6050_handle_t sensor, int16_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	uint8_t *buffer_t = malloc(sizeof(uint8_t *) * 10);
	esp_err_t ret;
	ret = i2c_bus_read_reg8(sens->i2c_dev, MPU6050_ZA_OFFS_H, 2, buffer_t);
	*buffer = (((int16_t)buffer_t[0]) << 8) | buffer_t[1];
	free(buffer_t);
	return ret;
}

esp_err_t mpu6050_set_z_acce_offset(mpu6050_handle_t sensor, int16_t buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_write_reg8(sens->i2c_dev, MPU6050_ZA_OFFS_H, 2, (uint8_t *)&buffer);
	return ret;
}

esp_err_t mpu6050_get_x_gyro_offset(mpu6050_handle_t sensor, int8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	uint8_t *buffer_t = malloc(sizeof(uint8_t *) * 10);
	esp_err_t ret;
	ret = i2c_bus_read_bits(sens->i2c_dev, MPU6050_XG_OFFS_TC, MPU6050_TC_OFFSET_BIT,
				MPU6050_TC_OFFSET_LENGTH, buffer_t);
	*buffer = buffer_t[0];
	free(buffer_t);
	return ret;
}

esp_err_t mpu6050_set_x_gyro_offset(mpu6050_handle_t sensor, int8_t buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_write_bits(sens->i2c_dev, MPU6050_XG_OFFS_TC, MPU6050_TC_OFFSET_BIT,
				 MPU6050_TC_OFFSET_LENGTH, buffer);
	return ret;
}

esp_err_t mpu6050_get_y_gyro_offset(mpu6050_handle_t sensor, int8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	uint8_t *buffer_t = malloc(sizeof(uint8_t *) * 10);
	esp_err_t ret;
	ret = i2c_bus_read_bits(sens->i2c_dev, MPU6050_YG_OFFS_TC, MPU6050_TC_OFFSET_BIT,
				MPU6050_TC_OFFSET_LENGTH, buffer_t);
	*buffer = buffer_t[0];
	free(buffer_t);
	return ret;
}

esp_err_t mpu6050_set_y_gyro_offset(mpu6050_handle_t sensor, int8_t buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_write_bits(sens->i2c_dev, MPU6050_YG_OFFS_TC, MPU6050_TC_OFFSET_BIT,
				 MPU6050_TC_OFFSET_LENGTH, buffer);
	return ret;
}

esp_err_t mpu6050_get_z_gyro_offset(mpu6050_handle_t sensor, int8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	uint8_t *buffer_t = malloc(sizeof(uint8_t *) * 10);
	esp_err_t ret;
	ret = i2c_bus_read_bits(sens->i2c_dev, MPU6050_ZG_OFFS_TC, MPU6050_TC_OFFSET_BIT,
				MPU6050_TC_OFFSET_LENGTH, buffer_t);
	*buffer = buffer_t[0];
	free(buffer_t);
	return ret;
}

esp_err_t mpu6050_set_z_gyro_offset(mpu6050_handle_t sensor, int8_t buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_write_bits(sens->i2c_dev, MPU6050_ZG_OFFS_TC, MPU6050_TC_OFFSET_BIT,
				 MPU6050_TC_OFFSET_LENGTH, buffer);
	return ret;
}

esp_err_t mpu6050_get_clock_source(mpu6050_handle_t sensor, uint8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_read_bits(sens->i2c_dev, MPU6050_PWR_MGMT_1, MPU6050_PWR1_CLKSEL_BIT,
				MPU6050_PWR1_CLKSEL_LENGTH, buffer);
	return ret;
}

esp_err_t mpu6050_set_clock_source(mpu6050_handle_t sensor, uint8_t buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_write_bits(sens->i2c_dev, MPU6050_PWR_MGMT_1, MPU6050_PWR1_CLKSEL_BIT,
				 MPU6050_PWR1_CLKSEL_LENGTH, buffer);
	return ret;
}

esp_err_t mpu6050_get_full_scale_acce_range_id(mpu6050_handle_t sensor, uint8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_read_bits(sens->i2c_dev, MPU6050_ACCEL_CONFIG, MPU6050_ACONFIG_AFS_SEL_BIT,
				MPU6050_ACONFIG_AFS_SEL_LENGTH, buffer);
	return ret;
}

esp_err_t mpu6050_get_full_scale_acce_range_gpl(mpu6050_handle_t sensor, float *buffer)
{
	uint8_t *range_id = malloc(sizeof(uint8_t *) * 2);
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = mpu6050_get_full_scale_acce_range_id(sens, range_id);
	switch (range_id[0]) {
	case MPU6050_ACCEL_FS_2:
		*buffer = MPU6050_G_PER_LSB_2;
		break;
	case MPU6050_ACCEL_FS_4:
		*buffer = MPU6050_G_PER_LSB_4;
		break;
	case MPU6050_ACCEL_FS_8:
		*buffer = MPU6050_G_PER_LSB_8;
		break;
	case MPU6050_ACCEL_FS_16:
		*buffer = MPU6050_G_PER_LSB_16;
		break;
	default:
		*buffer = MPU6050_DEG_PER_LSB_1000;
		break;
	}
	free(range_id);
	return ret;
}

esp_err_t mpu6050_get_full_scale_gyro_range_id(mpu6050_handle_t sensor, uint8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_read_bits(sens->i2c_dev, MPU6050_GYRO_CONFIG, MPU6050_GCONFIG_FS_SEL_BIT,
				MPU6050_GCONFIG_FS_SEL_LENGTH, buffer);
	return ret;
}

esp_err_t mpu6050_get_full_scale_gyro_range_dpl(mpu6050_handle_t sensor, float *buffer)
{
	uint8_t *range_id = malloc(sizeof(uint8_t *) * 2);
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = mpu6050_get_full_scale_gyro_range_id(sens, range_id);
	switch (range_id[0]) {
	case MPU6050_GYRO_FS_250:
		*buffer = MPU6050_DEG_PER_LSB_250;
		break;
	case MPU6050_GYRO_FS_500:
		*buffer = MPU6050_DEG_PER_LSB_500;
		break;
	case MPU6050_GYRO_FS_1000:
		*buffer = MPU6050_DEG_PER_LSB_1000;
		break;
	case MPU6050_GYRO_FS_2000:
		*buffer = MPU6050_DEG_PER_LSB_2000;
		break;
	default:
		*buffer = MPU6050_DEG_PER_LSB_1000;
		break;
	}
	free(range_id);
	return ret;
}

void mpu6050_check_settings(mpu6050_handle_t sensor)
{
	printf("/********** sensor settings **********/\n");

	uint8_t *buffer = malloc(sizeof(uint8_t *) * 14);

	if (mpu6050_get_sleep_enable(sensor, buffer) == ESP_FAIL)
		return;
	printf("Sleep Mode: %s\n", buffer[0] ? "Enabled" : "Disabled");

	if (mpu6050_get_int_motion_enabled(sensor, buffer) == ESP_FAIL)
		return;
	printf("Motion Interrupt: %s\n", buffer[0] ? "Enabled" : "Disabled");

	if (mpu6050_get_int_zero_motion_enabled(sensor, buffer) == ESP_FAIL)
		return;
	printf("Zero Motion Interrupt: %s\n", buffer[0] ? "Enabled" : "Disabled");

	if (mpu6050_get_int_freefall_enable(sensor, buffer) == ESP_FAIL)
		return;
	printf("Free fall Interrupt: %s\n", buffer[0] ? "Enabled" : "Disabled");

	if (mpu6050_get_motion_detection_threshold(sensor, buffer) == ESP_FAIL)
		return;
	printf("Motion detection threshold: %d\n", buffer[0]);

	if (mpu6050_get_motion_detection_duration(sensor, buffer) == ESP_FAIL)
		return;
	printf("Motion detection duration: %d\n", buffer[0]);

	if (mpu6050_get_zero_motion_detection_threshold(sensor, buffer) == ESP_FAIL)
		return;
	printf("Zero motion detection threshold: %d\n", buffer[0]);

	if (mpu6050_get_zero_motion_detection_duration(sensor, buffer) == ESP_FAIL)
		return;
	printf("Zero motion detection duration: %d\n", buffer[0]);

	if (mpu6050_get_clock_source(sensor, buffer) == ESP_FAIL)
		return;
	printf("Clock Source: ");
	switch (buffer[0]) {
	case MPU6050_CLOCK_KEEP_RESET:
		printf("Stops the clock and keeps the timing generator in reset\n");
		break;
	case MPU6050_CLOCK_PLL_EXT19M:
		printf("PLL with external 19.2MHz reference\n");
		break;
	case MPU6050_CLOCK_PLL_EXT32K:
		printf("PLL with external 32.768kHz reference\n");
		break;
	case MPU6050_CLOCK_PLL_ZGYRO:
		printf("PLL with Z axis gyroscope reference\n");
		break;
	case MPU6050_CLOCK_PLL_YGYRO:
		printf("PLL with Y axis gyroscope reference\n");
		break;
	case MPU6050_CLOCK_PLL_XGYRO:
		printf("PLL with X axis gyroscope reference\n");
		break;
	case MPU6050_CLOCK_INTERNAL:
		printf("Internal 8MHz oscillator\n");
		break;
	}

	if (mpu6050_get_acce_poweron_delay(sensor, buffer) == ESP_FAIL)
		return;
	printf("Accelerometer power delay: %d ms\n", buffer[0]);

	if (mpu6050_get_full_scale_acce_range_id(sensor, buffer) == ESP_FAIL)
		return;
	printf("Accelerometer Full scale: ");
	switch (buffer[0]) {
	case MPU6050_ACCEL_FS_2:
		printf("+/- 2 g\n");
		break;
	case MPU6050_ACCEL_FS_4:
		printf("+/- 4 g\n");
		break;
	case MPU6050_ACCEL_FS_8:
		printf("+/- 8 g\n");
		break;
	case MPU6050_ACCEL_FS_16:
		printf("+/- 16 g\n");
		break;
	}

	float temp = 0.0f;
	if (mpu6050_get_full_scale_acce_range_gpl(sensor, &temp) == ESP_FAIL)
		return;
	printf("Accelerometer G per LBS: %2.9f\n", temp);

	if (mpu6050_get_full_scale_gyro_range_id(sensor, buffer) == ESP_FAIL)
		return;
	printf("Gyroscope Full scale: ");
	switch (buffer[0]) {
	case MPU6050_GYRO_FS_250:
		printf("+/- 250 DPS g\n");
		break;
	case MPU6050_GYRO_FS_500:
		printf("+/- 500 DPS g\n");
		break;
	case MPU6050_GYRO_FS_1000:
		printf("+/- 1000 DPS g\n");
		break;
	case MPU6050_GYRO_FS_2000:
		printf("+/- 2000 DPS g\n");
		break;
	}
	if (mpu6050_get_full_scale_gyro_range_dpl(sensor, &temp) == ESP_FAIL)
		return;
	printf("Gyroscope degrees per LBS: %2.9f\n", temp);
	printf("/********** sensor settings **********/\n");
	free(buffer);
}

esp_err_t mpu6050_get_int_dmp_enabled(mpu6050_handle_t sensor, uint8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_read_bit(sens->i2c_dev, MPU6050_INT_ENABLE, MPU6050_INTERRUPT_DMP_INT_BIT,
			       buffer);
	return ret;
}

esp_err_t mpu6050_set_int_dmp_enabled(mpu6050_handle_t sensor, bool enabled)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_write_bit(sens->i2c_dev, MPU6050_INT_ENABLE, MPU6050_INTERRUPT_DMP_INT_BIT,
				enabled);
	return ret;
}

esp_err_t mpu6050_get_int_dmp_int5_status(mpu6050_handle_t sensor, uint8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_read_bit(sens->i2c_dev, MPU6050_DMP_INT_STATUS, MPU6050_DMPINT_5_BIT, buffer);
	return ret;
}

esp_err_t mpu6050_get_int_dmp_int4_status(mpu6050_handle_t sensor, uint8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_read_bit(sens->i2c_dev, MPU6050_DMP_INT_STATUS, MPU6050_DMPINT_4_BIT, buffer);
	return ret;
}

esp_err_t mpu6050_get_int_dmp_int3_status(mpu6050_handle_t sensor, uint8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_read_bit(sens->i2c_dev, MPU6050_DMP_INT_STATUS, MPU6050_DMPINT_3_BIT, buffer);
	return ret;
}

esp_err_t mpu6050_get_int_dmp_int2_status(mpu6050_handle_t sensor, uint8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_read_bit(sens->i2c_dev, MPU6050_DMP_INT_STATUS, MPU6050_DMPINT_2_BIT, buffer);
	return ret;
}

esp_err_t mpu6050_get_int_dmp_int1_status(mpu6050_handle_t sensor, uint8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_read_bit(sens->i2c_dev, MPU6050_DMP_INT_STATUS, MPU6050_DMPINT_1_BIT, buffer);
	return ret;
}

esp_err_t mpu6050_get_int_dmp_int0_status(mpu6050_handle_t sensor, uint8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_read_bit(sens->i2c_dev, MPU6050_DMP_INT_STATUS, MPU6050_DMPINT_0_BIT, buffer);
	return ret;
}

esp_err_t mpu6050_get_int_dmp_status(mpu6050_handle_t sensor, uint8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_read_bit(sens->i2c_dev, MPU6050_INT_STATUS, MPU6050_INTERRUPT_DMP_INT_BIT,
			       buffer);
	return ret;
}

esp_err_t mpu6050_get_dmp_enabled(mpu6050_handle_t sensor, uint8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_read_bit(sens->i2c_dev, MPU6050_USER_CTRL, MPU6050_USERCTRL_DMP_EN_BIT,
			       buffer);
	return ret;
}

esp_err_t mpu6050_set_dmp_enabled(mpu6050_handle_t sensor, bool enabled)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_write_bit(sens->i2c_dev, MPU6050_USER_CTRL, MPU6050_USERCTRL_DMP_EN_BIT,
				enabled);
	return ret;
}

esp_err_t mpu6050_get_x_neg_motion_detected(mpu6050_handle_t sensor, uint8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_read_bit(sens->i2c_dev, MPU6050_MOT_DETECT_STATUS,
			       MPU6050_MOTION_MOT_XNEG_BIT, buffer);
	return ret;
}

esp_err_t mpu6050_get_x_pos_motion_detected(mpu6050_handle_t sensor, uint8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_read_bit(sens->i2c_dev, MPU6050_MOT_DETECT_STATUS,
			       MPU6050_MOTION_MOT_XPOS_BIT, buffer);
	return ret;
}

esp_err_t mpu6050_get_y_neg_motion_detected(mpu6050_handle_t sensor, uint8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_read_bit(sens->i2c_dev, MPU6050_MOT_DETECT_STATUS,
			       MPU6050_MOTION_MOT_YNEG_BIT, buffer);
	return ret;
}

esp_err_t mpu6050_get_y_pos_motion_detected(mpu6050_handle_t sensor, uint8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_read_bit(sens->i2c_dev, MPU6050_MOT_DETECT_STATUS,
			       MPU6050_MOTION_MOT_YPOS_BIT, buffer);
	return ret;
}

esp_err_t mpu6050_get_z_neg_motion_detected(mpu6050_handle_t sensor, uint8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_read_bit(sens->i2c_dev, MPU6050_MOT_DETECT_STATUS,
			       MPU6050_MOTION_MOT_ZNEG_BIT, buffer);
	return ret;
}

esp_err_t mpu6050_get_z_pos_motion_detected(mpu6050_handle_t sensor, uint8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_read_bit(sens->i2c_dev, MPU6050_MOT_DETECT_STATUS,
			       MPU6050_MOTION_MOT_ZPOS_BIT, buffer);
	return ret;
}

esp_err_t mpu6050_set_dlpf_mode(mpu6050_handle_t sensor, bool enabled)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_write_bits(sens->i2c_dev, MPU6050_CONFIG, MPU6050_CFG_DLPF_CFG_BIT,
				 MPU6050_CFG_DLPF_CFG_LENGTH, enabled);
	return ret;
}

esp_err_t mpu6050_get_dlpf_mode(mpu6050_handle_t sensor, uint8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_read_bits(sens->i2c_dev, MPU6050_CONFIG, MPU6050_CFG_DLPF_CFG_BIT,
				MPU6050_CFG_DLPF_CFG_LENGTH, buffer);
	return ret;
}

esp_err_t mpu6050_set_sample_rate(mpu6050_handle_t sensor, uint8_t buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_write_byte(sens->i2c_dev, MPU6050_SMPLRT_DIV, buffer);
	return ret;
}

esp_err_t mpu6050_get_sample_rate(mpu6050_handle_t sensor, uint8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_read_byte(sens->i2c_dev, MPU6050_SMPLRT_DIV, buffer);
	return ret;
}

esp_err_t mpu6050_get_temp_fifo_enabled(mpu6050_handle_t sensor, uint8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_read_bit(sens->i2c_dev, MPU6050_FIFO_EN, MPU6050_TEMP_FIFO_EN_BIT, buffer);
	return ret;
}

esp_err_t mpu6050_set_temp_fifo_enabled(mpu6050_handle_t sensor, bool enabled)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_write_bit(sens->i2c_dev, MPU6050_FIFO_EN, MPU6050_TEMP_FIFO_EN_BIT, enabled);
	return ret;
}

esp_err_t mpu6050_get_x_gyro_fifo_enabled(mpu6050_handle_t sensor, uint8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_read_bit(sens->i2c_dev, MPU6050_FIFO_EN, MPU6050_XG_FIFO_EN_BIT, buffer);
	return ret;
}

esp_err_t mpu6050_set_x_gyro_fifo_enabled(mpu6050_handle_t sensor, bool enabled)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_write_bit(sens->i2c_dev, MPU6050_FIFO_EN, MPU6050_XG_FIFO_EN_BIT, enabled);
	return ret;
}

esp_err_t mpu6050_get_y_gyro_fifo_enabled(mpu6050_handle_t sensor, uint8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_read_bit(sens->i2c_dev, MPU6050_FIFO_EN, MPU6050_YG_FIFO_EN_BIT, buffer);
	return ret;
}

esp_err_t mpu6050_set_y_gyro_fifo_enabled(mpu6050_handle_t sensor, bool enabled)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_write_bit(sens->i2c_dev, MPU6050_FIFO_EN, MPU6050_YG_FIFO_EN_BIT, enabled);
	return ret;
}

esp_err_t mpu6050_get_z_gyro_fifo_enabled(mpu6050_handle_t sensor, uint8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_read_bit(sens->i2c_dev, MPU6050_FIFO_EN, MPU6050_ZG_FIFO_EN_BIT, buffer);
	return ret;
}

esp_err_t mpu6050_set_z_gyro_fifo_enabled(mpu6050_handle_t sensor, bool enabled)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_write_bit(sens->i2c_dev, MPU6050_FIFO_EN, MPU6050_ZG_FIFO_EN_BIT, enabled);
	return ret;
}

esp_err_t mpu6050_get_acce_fifo_enabled(mpu6050_handle_t sensor, uint8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_read_bit(sens->i2c_dev, MPU6050_FIFO_EN, MPU6050_ACCEL_FIFO_EN_BIT, buffer);
	return ret;
}

esp_err_t mpu6050_set_acce_fifo_enabled(mpu6050_handle_t sensor, bool enabled)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_write_bit(sens->i2c_dev, MPU6050_FIFO_EN, MPU6050_ACCEL_FIFO_EN_BIT, enabled);
	return ret;
}

esp_err_t mpu6050_write_mem(mpu6050_handle_t sensor, unsigned short mem_addr, unsigned short length,
			    uint8_t *data)
{
	uint8_t temp[2];
	if (!data)
		return ESP_FAIL;

	temp[0] = (uint8_t)(mem_addr >> 8);
	temp[1] = (uint8_t)(mem_addr & 0xFF);

	if (temp[1] + length > BANK_SIZE)
		return ESP_FAIL;

	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_write_bytes(sens->i2c_dev, MPU6050_BANK_SEL, 2, temp);
	if (ret != ESP_OK)
		return ret;
	ret = i2c_bus_write_bytes(sens->i2c_dev, MPU6050_MEM_R_W, length, data);
	if (ret != ESP_OK)
		return ret;

	return ESP_OK;
}

esp_err_t mpu6050_read_mem(mpu6050_handle_t sensor, unsigned short mem_addr, unsigned short length,
			   uint8_t *data)
{
	uint8_t temp[2];
	if (!data)
		return ESP_FAIL;

	temp[0] = (uint8_t)(mem_addr >> 8);
	temp[1] = (uint8_t)(mem_addr & 0xFF);

	if (temp[1] + length > BANK_SIZE)
		return ESP_FAIL;

	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_write_bytes(sens->i2c_dev, MPU6050_BANK_SEL, 2, temp);
	if (ret != ESP_OK)
		return ret;
	ret = i2c_bus_read_bytes(sens->i2c_dev, MPU6050_MEM_R_W, length, data);
	if (ret != ESP_OK)
		return ret;
	return ESP_OK;
}

esp_err_t mpu6050_get_interrupt_mode(mpu6050_handle_t sensor, uint8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_read_bit(sens->i2c_dev, MPU6050_INT_PIN_CFG, MPU6050_INTCFG_INT_LEVEL_BIT,
			       buffer);
	return ret;
}

esp_err_t mpu6050_set_interrupt_mode(mpu6050_handle_t sensor, uint8_t buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_write_bit(sens->i2c_dev, MPU6050_INT_PIN_CFG, MPU6050_INTCFG_INT_LEVEL_BIT,
				buffer);
	return ret;
}

esp_err_t mpu6050_get_interrupt_latch(mpu6050_handle_t sensor, uint8_t *buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_read_bit(sens->i2c_dev, MPU6050_INT_PIN_CFG, MPU6050_INTCFG_LATCH_INT_EN_BIT,
			       buffer);
	return ret;
}

esp_err_t mpu6050_set_interrupt_latch(mpu6050_handle_t sensor, uint8_t buffer)
{
	mpu6050_dev_t *sens = (mpu6050_dev_t *)sensor;
	esp_err_t ret;
	ret = i2c_bus_write_bit(sens->i2c_dev, MPU6050_INT_PIN_CFG, MPU6050_INTCFG_LATCH_INT_EN_BIT,
				buffer);
	return ret;
}
